---
layout: markdown_page
title: "Category Direction - Continuous Integration"
description: "Continuous Integration is an important part of any software development pipeline. It must be easy to use, reliable, and accurate. Learn more here!"
canonical_path: "/direction/verify/continuous_integration/"
---

- TOC
{:toc}

## Continuous Integration

Continuous Integration is an important part of any software development pipeline, and part of the [Verify stage](/direction/ops/#verify) here at GitLab. CI must be easy to use, reliable, and accurate in terms of results, so that's the core of where we focus. While we are very proud that we are recognized as [the leading CI/CD tool on the market](/blog/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/), as well as a leader in the 2019 Q3 [Cloud Native CI Wave](/resources/forrester-wave-cloudnative-ci/), it's important for us that we continue to innovate in this area and provide not just a "good enough" solution, but a great one.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration)
- [Topic Labels](https://gitlab.com/groups/gitlab-org/-/labels?utf8=%E2%9C%93&subscribed=&search=ci%3A%3A) (because CI is such a large category, it is broken out into topic areas)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)
- [JTBD overview](/direction/verify/continous_integration/jobs_to_be_done)

You may also be looking for one of the following related product direction pages: [GitLab Runner](/direction/verify/runner/), [Continuous Delivery](/direction/release/continuous_delivery/), [Release stage](/direction/ops#release), or [Jenkins Importer](/direction/verify/jenkins_importer).

## What's Next & Why

We're working now on delivering our most popular customer issue, the ability to keep only the latest artifact in each branch ([gitlab#16267](https://gitlab.com/gitlab-org/gitlab/issues/16267)).

Our new (dynamic) child/parent pipelines feature is also getting some attention with three major MVCs in progress:

- [Seamless child/parent pipeline web experience MVC](https://gitlab.com/groups/gitlab-org/-/epics/4021)
- [MVC for easier dynamic, runtime generation of `.gitlab-ci.yml`](https://gitlab.com/groups/gitlab-org/-/epics/4020)
- [Seamless artifact handling for child/parent pipelines MVC](https://gitlab.com/groups/gitlab-org/-/epics/4019)

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our [definitions of maturity levels](/direction/maturity/)), it is important to us that we defend that position in the market. As such, we are balancing prioritization of [important P2 issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=P2) and [items from our backlog of popular smaller feature requests](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Acontinuous+integration&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93) in addition to delivering new features that move the vision forward. If you feel there are any gaps or items that would risk making GitLab no longer lovable for you, please let us know.

## Competitive Landscape

### GitHub CI/CD (Actions)

GitHub has evolved Actions into more and more of a standalone CI/CD solution. GitLab remains far ahead in a lot of areas of CI/CD that they are going to have to catch up on, but Microsoft and GitHub have a lot of resources and have a large user base excited to use their product, especially when given away as part of a package. Actions has a more event-driven and community plugin-based approach than GitLab, which is more declarative and where we take community contributions directly into the product where they can be maintained, rather than something closer to the Jenkins plugin model with community maintained Actions.

Time will tell if distributed CI ends up being more powerful, or just harder to reason about, and if they are able to avoid the same trap of unmaintained community Actions that companies became dependent on that Jenkins ran into. Regardless, we're monitoring and bringing the best of their innovation into our product. We've already had some successes [running GitHub Actions directly in GitLab CI](https://gitlab.com/snippets/1988376) and will continue to explore that. We are also working on a [migration guide](https://gitlab.com/gitlab-org/gitlab/-/issues/228937) to help teams we're hearing from who are looking to move over to GitLab have an easier time. Features like [making the script section in containers optional](https://gitlab.com/gitlab-org/gitlab/-/issues/223203) would make it easier to build reusable plugins within GitLab that would behave more like Actions.

### Jenkins

Jenkins is largely seen as a legacy tool, and most people we speak with are interested in moving off to something more modern. We are addressing this with our [Jenkins Importer](/direction/verify/jenkins_importer) category which is designed to make this as easy as possible.

### Other solutions

There are other CI solutions in the market, but the majority of the conversation is between us, Jenkins, and Actions at this point. Atlassian has built BitBucket Pipelines, a more modernized version of Bamboo, which is still in the early stages. Microsoft is maintaining (at least for now) Azure DevOps at the same time as GitHub. CodeFresh and CircleCI have both released [container-based plugin model](https://steps.codefresh.io/), similar to GitHub Actions. CircleCI in particular is known for very fast startup times and we're looking to ensure we [keep up or get even faster](https://gitlab.com/groups/gitlab-org/-/epics/439).

## Analyst Landscape

There are a few key findings from the Forrester Research analysts on our CI solution. GitLab is seen as capable as the solutions provided by the hyperclouds themselves, and well ahead of other neutral solutions. This can give our users flexibility when it comes to which cloud provider(s) they want to use. We are also seen as the best end to end leader, with other products  not keeping up and not providing as comprehensive solutions. What this tells us is that it is important for us to continue to innovate and make it hard or even impossible for competitors to maintain pace.

As such, our path to improving our analyst performance matches our solutions above in terms of staying ahead of our competitors.

## Top Customer Success/Sales Issue(s)

The most popular Customer Success issues as determined in FQ1-20 survey of the Technical Account Managers was [filtering pipelines by status or branch](https://gitlab.com/groups/gitlab-org/-/epics/3286). Also important for the sales team is [gitlab#205494](https://gitlab.com/gitlab-org/gitlab/issues/205494) which will allow for easier use of GitLab's security features when not using GitLab's CI.

In addition to features, our sales team has requested a Jenkins importer in order to make [migrating to GitLab](https://gitlab.com/groups/gitlab-org/-/epics/2072) easier: this is being delivered via the [Jenkins Importer](/direction/verify/jenkins_importer) category, but is mentioned here for completeness.

## Top Customer Issue(s)

Our top five customer issues ([search](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3AContinuous+Integration&label_name%5B%5D=customer&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)) include the following:

- [Keep latest artifacts for the last successful jobs](https://gitlab.com/gitlab-org/gitlab/-/issues/16267)
- [Ensure after_script is called for cancelled and timed out pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/15603)
- [Make it possible to control build status using exit codes](https://gitlab.com/gitlab-org/gitlab/-/issues/16733)
- [Extend deploy tokens to multiple projects](https://gitlab.com/gitlab-org/gitlab/-/issues/21766)
- [Allow `needs:` (DAG) to refer to a job in the same stage](https://gitlab.com/gitlab-org/gitlab/-/issues/30632)

Another item with a lot of attention is to normalize job tokens in a more flexible way, so that they can have powerful abilities when needed and still not introduce security risks ([gitlab#3559](https://gitlab.com/groups/gitlab-org/-/epics/3559)).

We also have a few issues about making variables available before includes are processed, however there is a "chicken and egg" problem here that has been difficult to solve. Child/parent pipelines solves some use cases, but not all, and in the meantime we are continuing the discussion in the issue [gitlab#1809](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809). If you're interested in technical discussion around the challenges and want to participate in solving them, please see the conversation [here](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809#note_225636231). There are two related epics here, [Use a variable inside other variables in .gitlab-ci.yml](https://gitlab.com/groups/gitlab-org/-/epics/3589) and [Raw (unexpanded) variables MVC](https://gitlab.com/groups/gitlab-org/-/epics/1994)

## Top Internal Customer Issue(s)

Our top five internal customer issues ([search](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration&label_name[]=internal%20customer)) include the following:

- [Allow `needs:` (DAG) to refer to a job in the same stage](https://gitlab.com/gitlab-org/gitlab/-/issues/30632)
- [Recognise links to urls and repository files in job logs](https://gitlab.com/gitlab-org/gitlab/-/issues/18324)
- [Pipeline doesn't succeed when manual jobs using new DAG dependency "needs:" are waiting for other "when: manual" jobs to succeed](https://gitlab.com/gitlab-org/gitlab/-/issues/31264)
- [Cannot use `$` character in build variables](https://gitlab.com/gitlab-org/gitlab/-/issues/17069)
- [Add support for `when:manual` within triggered pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/201938)

Our top three dogfooding issues ([search](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration&label_name[]=Dogfooding)) are:

- [Group level pipeline dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/11960)
- [Segregate access control regarding running pipelines from ability to push/merge](https://gitlab.com/gitlab-org/gitlab/-/issues/24585)
- [Public pipeline page MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/10861)

## Top Vision Item(s)

With the beta release of the [Directed Acyclic Graph (DAG) pipeline visualization](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/#dag-visualization), we are now collecting feedback in [gitlab#220368](https://gitlab.com/gitlab-org/gitlab/-/issues/220368) for the next iteration of this feature for making it easier to trace the relationship between dependent jobs. We have a set of other follow ups as well for the DAG [gitlab-org#1716](https://gitlab.com/groups/gitlab-org/-/epics/1716), which will allow for out of order execution and open a world of new possibilities around how pipelines are constructed.

We have an epic to improve the `rules` syntax at [gitlab-org#2783](https://gitlab.com/groups/gitlab-org/-/epics/2783), as well as one for making [improvements to the CI Linter](https://gitlab.com/groups/gitlab-org/-/epics/3517) to provide more helpful warnings and links to documentation. Making it [easier to use CI with external repositories](https://gitlab.com/groups/gitlab-org/-/epics/1418) is also on the radar. We also have an epic focused on improving the [`.gitlab-ci.yml` authoring experience](https://gitlab.com/groups/gitlab-org/-/epics/1570) in a number of ways.

Looking further into the future, we have plans around supporting [ML/AI technologies as part of (or being built by) GitLab CI](https://gitlab.com/groups/gitlab-org/-/epics/2436).
