---
layout: job_family_page
title: "Product Designer"
---

# Product Design Roles at GitLab

At GitLab, Product Designers collaborate closely with Product Managers, Engineers, UX Research, and other Product Designers to create a [productive, minimal, and human][pajamas] experience. Product Designers report to a UX Manager.

## Responsibilities

* Help to define and improve the interface and experience of GitLab.
* Design features that fit within the larger experience and flows.
* Create deliverables (wireframes, mockups, prototypes, flows, and so on) to communicate ideas.
* Work with Product Managers and Engineers to iterate on and refine the existing experience.
* Involve UX Researchers when needed, and help them define research initiatives (usability tests, surveys, interviews, and so on).
* Stay informed and share the latest on UI/UX techniques, tools, and patterns.
* Understand responsive design and best practices.
* Have working knowledge of HTML, CSS. Familiarity with Javascript.
* Have knowledge and understanding of design systems theory and practice.
* Have a general knowledge of Git flow (for example, feature branching, merge/pull requests, pipelines, and code testing).

### Product Designer

* Deeply understand the technology and features of the product area to which you are assigned.
* Conduct [solution validation](/handbook/product-development-flow/#validation-phase-4-solution-validation) with guidance from your Product Design Manager, and incorporate insights into design decisions to fulfill user and business needs.
* Create tactical and strategic deliverables for your area (for example, wireframes, competitive analyses, prototypes, journey maps, storyboards, personas, and so on).
* Communicate the results of UX activities within your product area to the UX department, cross-functional partners within your product area, and other interested GitLab team members using clear language that simplifies complexity.
* Proactively identify both small and large usability issues within your product area.
* Take part in the [monthly release process](/handbook/engineering/workflow/#product-development-timeline) by breaking down designs to fit release cadence, and review and approve merge requests submitted by developers.
* Actively contribute to the [Pajamas Design System][pajamas] to keep design components available and up to date in prototyping tools, ensuring that reusable components fit visually and functionally together.
* Participate in [Design Reviews](/handbook/engineering/ux/ux-designer/#design-reviews), giving and receiving feedback in an appropriate way.
* Understand UX debt and make recommendations for its resolution.

#### Job Grade

The Product Designer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Senior Product Designer

Everything in the Product Designer role, plus:

* Have working knowledge of the product area to which you are assigned, and proactively learn other product areas.
* Independently conduct [solution validation](/handbook/product-development-flow/#validation-phase-4-solution-validation) with minimal guidance from your Product Design Manager, and incorporate insights into design decisions to fulfill user and business needs.
* Proactively identify both small and large usability issues within your product area, and help influence your Product Design Manager and Product Manager to prioritize them.
* Model best practices for giving and receiving feedback:
  - by participating in and holding [Design Reviews](/handbook/engineering/ux/ux-designer/#design-reviews)
  - in [Weekly 1:1s](/handbook/leadership/1-1/) with your manager
  - through UX issues
* Actively contribute to the [Pajamas Design System][pajamas], help determine whether components are single-use or multi-use, and provide recommendations to designers regarding new component requests.
* Mentor other members of the UX department, both inside and outside of your product area.
* Support your Product Design Manager and Product Manager in identifying dependencies between stages and advocating for cross-stage collaboration when needed.
* Engage in social media efforts, including writing blog articles, giving talks, and responding on Twitter, as appropriate.
* Interview potential UX candidates.

#### Job Grade

The Senior Product Designer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Staff Product Designer

Everything in the Senior Product Designer role, plus:

* Drive cross-stage collaboration by helping designers to identify dependencies and areas for cross-department work.
* Actively contribute handbook changes that help the UX Department evolve the culture and best practices.
* Understand the nuances and considerations between problem and solution validation, and mentor other designers on how they plan research.
* Drive innovation across the organization by increasing the adoption of new processes and tools.
* Mentor other designers in deciding which deliverables and approach are most valuable at each stage of validation, and create those artifacts with them (for example, personas at the problem stage, storyboards at the solution stage).
* Push forward the craft of product design in regards to our standards and approaches in an all-remote context, fostering a design studio atmosphere within GitLab.
* Exert influence on the overall objectives and long-range goals of your section by collaborating with Product and Engineering on the Vision pages.
* Set an example for how to effectively communicate across stages by frequently participating in asynchronous collaboration in issues and merge requests.
* Teach and socialize decision-making frameworks to the GitLab community, so that designers can solve problems more efficiently on their own.

#### Job Grade

The Staff Product Designer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### FE/UX Foundations

The Foundations team works on building a cohesive and consistent user experience, both visually and functionally. You'll be responsible for leading the direction of the experience design, visual style, and technical tooling of the GitLab product. You'll act as a centralized resource, helping to triage large-scale experience problems as the need arises.

You’ll spend your time collaborating with a [cross-functional team](https://about.gitlab.com/handbook/product/product-categories/#ecosystem-group), helping to implement our [Design System](https://design.gitlab.com/), building comprehensive accessibility standards into our workflows, and defining guidelines and best practices that will inform how teams are designing and building products. A breakdown of the vision you’ll be helping to deliver within the FE/UX Foundation category can be found on our [product direction page](https://about.gitlab.com/direction/create/ecosystem/frontend-ux-foundations/).

**What you can expect in a Product Designer, FE/UX Foundations role at GitLab**

* Deeply understand the technology and features of the Foundations category, including our [Pajamas Design System](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com), our [GitLab UI component repository](https://gitlab.com/gitlab-org/gitlab-ui), our [GitLab Design repository](https://gitlab.com/gitlab-org/gitlab-design), and our [GitLab SVGs library](https://gitlab.com/gitlab-org/gitlab-svgs).
* Provide comprehensive usage guidelines, reusable components, content standards, and usability documentation to the greater GitLab organization and the [GitLab community](https://docs.google.com/presentation/d/1vMLivyv4ZPOTFmUk-c3etzGesRFaaLjdLzGM3SFVXT8/edit#slide=id.g29a70c6c35_0_68) as a whole.
* Become a centralized resource that understands all aspects of our design system, including which components exist within the GitLab product and what the component lifecycle looks like.
* Validate whether or not components and regions satisfy the needs of the product and its users with the help of your Product Design Manager.
* Validate the effectiveness of the design system by conducting solution validation with internal stakeholders, as well as end users of the product, with the help of your Product Design Manager.
* Create tactical and strategic deliverables to convey your design thinking and ideas, such as wireframes, prototypes, user flows, mockups, high-fidelity visual designs, or any other artifacts you believe are necessary.
* Communicate the results of your UX activities to the UX department, cross-functional partners, and other interested GitLab team-members using clear language that simplifies complexity.
* Socialize the work done by the Foundations team across stage groups by utilizing our weekly UX call and Engineering Week in Review document to ensure that other product areas can efficiently utilize the results.
* Proactively identify improvements to components or usability patterns that are negatively affecting the user experience, working alongside other Product Designers and Engineers to iteratively enhance them.
* Assess components for accessibility compliance and create issues for improvements.
* Identify areas of the [Pajamas Design System website](https://design.gitlab.com/) user experience that can be improved, such as improvements to the information architecture.
* Take part in the monthly release process by breaking down designs to fit release cadence, reviewing, and approving merge requests submitted by cross-functional counterparts.
* Keep components up to date, both within our [Pajamas Design System](https://design.gitlab.com/) and our [GitLab UI component repository](https://gitlab.com/gitlab-org/gitlab-ui), as well as our [Pajamas UI Kit](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Pajamas-UI-Kit---Beta) in Figma.
* Utilize GitLab's visual language to create components that visually fit together.
* Contribute to GitLab's visual language by creating icons and illustrations for other product areas as needed.
* Submit merge requests to fix minor bugs, such as broken links or spelling/grammar errors.
* Participate in Design Reviews by attending UX Showcases, and provide feedback to other designers afterwards when applicable.
* Provide and receive feedback on Foundations deliverables, both from those inside and outside of the team.
* Create issues that address UX debt both within the GitLab Product and the [Pajamas Design System](https://design.gitlab.com/), and advocate for them during the scheduling of a given release.

**What you can expect in a Senior Product Designer, FE/UX Foundations role at GitLab**

Everything in the Product Designer, FE/UX Foundations role plus:

* Proactively learn other product areas in order to propose design solutions that work for multiple use cases and scenarios across the product.
* Validate whether or not components and regions satisfy the needs of the product and its users with minimal help from your Product Design Manager.
* Validate the effectiveness of the design system by conducting solution validation with internal stakeholders, as well as end users of the product, with minimal help from your Product Design Manager.
* Propose process improvements that increase the efficiency of Product Designers and Engineers.
* Actively contribute to the improvement of our [Pajamas Design System](https://design.gitlab.com/) by submitting merge requests that improve functionality, enhance the user experience, and fix bugs.
* Support other designers by providing recommendations regarding new component requests, and help the greater UX department determine when a component is single-use or multi-use.
* Engage in social media efforts, including writing blog articles, giving talks, and responding on Twitter, as appropriate.
* Be a leader in the design community by sharing insights and decisions regarding our design system.
* Participate in the hiring processes by interviewing potential UX candidates.
* Exert influence over the category's direction by shaping the product goals and roadmap.
* Support your Product Design Manager and Product Manager in identifying dependencies between stages and advocating for cross-stage collaboration when needed
* Mentor other members of the UX department, both inside and outside of your team, by providing guidance on how to contribute to the design system.
* Help foster a community where everyone feels welcomed to contribute and be a part of our growing system.
* Advocate and teach other product designers about accessibility standards.

**What you can expect in a Staff Product Designer, FE/UX Foundations role at GitLab**

Everything in the Senior Product Designer, FE/UX Foundations role plus:

* Proactively learn other product areas in order to propose design solutions that work for multiple use cases and scenarios across the product, driving cross-stage collaboration to implement solutions.
* Validate whether or not components and regions satisfy the needs of the product and its users.
* Validate the effectiveness of the design system by conducting solution validation with internal stakeholders, as well as end users of the product.
* Understand the nuances and considerations between problem and solution validation, and mentor other designers on how they plan research.
* Assess components for accessibility compliance, create issues for improvements, and propose fixes.
* Exert significant influence over the category's direction by shaping the long-range product goals, roadmap, priorities, and strategy.
* Mentor other members of the UX department, both inside and outside of your team, by helping to determine which deliverables and approach are most valuable at each stage of validation. Help create artifacts with them where appropriate.
* Push forward the craft of product design in regards to our standards and approaches to design at GitLab in an all-remote context.
* Set an example for how to effectively communicate across stages by frequently participating in asynchronous collaboration in issues and merge requests.
* Teach and socialize decision-making frameworks to the GitLab community, so that designers can solve problems more efficiently on their own.
* Actively contribute handbook changes that help the organization evolve the culture and best practices.
* Drive innovation across the organization by driving the adoption of processes and tools.

## Tools

Tools used by the UX department are flexible depending on the needs of the work. Please see the [Product Designer Onboarding](/handbook/engineering/ux/uxdesigner-onboarding/) page for more details.

## Performance indicators
* [Perception of System Usability](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)
* [Ratio of Proactive vs Reactive UX Work](/handbook/engineering/ux/performance-indicators/#ratio-of-proactive-vs-reactive-ux-work)
* [Beautifying our UI](/handbook/engineering/ux/performance-indicators/#ui-beautification)
* [Ratio of Breadth vs Depth Work](/handbook/engineering/ux/performance-indicators/#ratio-of-breadth-vs-depth-work)
* [UX Debt](/handbook/engineering/ux/performance-indicators/#ux-debt)

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
- [UX Team](/handbook/engineering/ux/)
- [GitLab Design Kit](https://gitlab.com/gitlab-org/gitlab-design)
- [GitLab Design System][pajamas]

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their/their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one our Global Recruiters. In this call we will discuss your experience, understand what you are looking for in a Product Design role, talk about your work and approach to product design, discuss your compensation expectations, reasons why you want to join GitLab and answer any questions you have.
* Next, if a candidate successfully passes the screening call, candidates will be invited to schedule a 45 minute first interview with a Product Designer. In this interview, we will want you to talk through a case study in your portfolio so that we can find out about your process and understand your approach to design, your philosophy on design, and understand what you're looking for generally in a Product Designer position. This interview will also cover the more technical elements of design, so be prepared to talk about the tools and skills you use as a Designer.
* If you successfully pass the previous stage, candidates will then be invited to schedule a 1-hour interview with a UX Manager. In this interview, we will be looking for you to give some real insight into a problem you were solving as part of a project you've worked on. Ideally this case study will be from a project with similar challenges, goals, or context to the type of work you'll find in the [stage group](https://about.gitlab.com/handbook/product/product-categories/#devops-stages) you're interviewing for. We'll look to understand the size and structure of the team you were a part of, the goals of the project, your low-fidelity design work, your high-fidelity design skills, how you approached research, how you synthesised research data to inform design decisions, what design standards and guidelines you worked within, and how you collaborated with a wider team.  
* Candidates will be invited to schedule a third 50-minute interview with our UX Director if they successfully pass the previous interview. This interview will focus on assessing your research, strategy, and design skills. The interviewer will want to understand how you have incorporated research into your work and get a feel for your understanding of the fundamentals of research and UX methodology. Be prepared to answer questions around the soft skills Product Designers need, and be prepared to talk the interviewer through how you apply these skills in the real world.
* Finally, you will interview with a Product Manager who will focus on your ability to collaborate with Product teams and how your skills align with the needs of a specific stage group.
* Successful candidates will subsequently be made an offer via a video call or phone call.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

[groups]: /company/team/structure/#groups
[pajamas]: https://design.gitlab.com

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.
