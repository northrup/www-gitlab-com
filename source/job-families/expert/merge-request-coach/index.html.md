---
layout: markdown_page
title: "Merge Request Coach"
---

The main goal of a Merge Request Coach is to help
[merge requests from the community](https://gitlab.com/gitlab-org/gitlab/merge_requests?label_name[]=Community%20contribution)
get merged into GitLab.

## Responsibilities

* Triage merge requests labeled `~Community Contribution`.
* Close merge requests that we don't want, with a clear explanation on the
  reasons why, so that people don't feel discouraged.
* Help contributors to get their merge requests to meet the
  [contribution acceptance criteria](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#contribution-acceptance-criteria).
* Read the weekly `Merge requests requiring attention` email to follow-up on inactive MRs. Each coach should pick a few MRs to follow-up each week. 
* Help find and assign merge requests to available reviewers.
* If the contributor is unresponsive or if they are unable to finish it, finish
  their merge requests. Also, see the [closing policy for issues and merge requests](https://docs.gitlab.com/ee/development/contributing/#closing-policy-for-issues-and-merge-requests).
  1. Close the original merge request and say that you will finish it.
  1. Check out the branch locally.
  1. Make sure a changelog entry crediting the author exists.
  1. Add your own commits to improve and finish the original work.
  1. Push to a new branch and open a new merge request.
* Make it easy to contribute to GitLab even for people who are new to Ruby,
  JavaScript, Golang, etc. or programming entirely. For example, you can add any hints or possible fixes on issues that are open for community contribution.

## Collaboration guidelines

As a Merge Request Coach you will collaborate with people from the wider GitLab community and respond or close their merge requests per need.

### Responding to merge requests

With each merge request opened by a wider community member, it's important to note GitLab is **not** their main focus. They have contributed to GitLab out of kindness, and we should aim to give them the space they need to fulfill their merge request.

After a merge request from a wider community member has been submitted and you have provided feedback, allow a period of up to **two weeks** for the community member to continue their work before following up with the community member through a comment in the merge request.   

### Closing merge requests

Sometimes community contributions become stale or obsolete and changes become no longer relevant or applicable. If the changes are no longer needed, it's fine to close the merge request whether the author is responsive or not. If there’s an open discussion or questions for the author, allow some time for them to get back to the discussion before closing the merge request.

In all cases, **always** provide some context on why the merge request is being closed as this can lead to fewer questions later on and create a point for future reference which would be useful for team members and community contributors.

Last but not least, if there's an opportunity to provide any help or pointers for future contributions try to do that. This could be pointing to [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html), documentation on [how to contribute](https://docs.gitlab.com/ee/development/contributing/#how-to-contribute), or [getting help](/community/contribute/#getting-help) while [contributing](/community/contribute/) to GitLab.

More information on Merge Request Coach is available in the [handbook](/handbook/marketing/community-relations/code-contributor-program/resources/merge-request-coach-lifecycle.html). 
