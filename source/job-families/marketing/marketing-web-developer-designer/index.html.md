---
layout: job_family_page
title: "Digital Experience and Design"
---

As a Digital Experience Designer/Developer you will work closely with product marketing, content marketing, and other members of the Marketing team. This role will be part of the Marketing Design team. 

## Responsibilities

* Oversee design and development (HTML, CSS, JavaScript) of [about.gitlab.com](/), emails, and standalone landing pages and other digital media.
* Develop & maintain a top-notch user experience across all of these properties through A/B testing and data-guided design.
* Research, design, and implement concepts that increase engagement and conversion.
* Define personas, user flows, design systems and style guides, and front-end framework(s) to drive efficiency and consistency in our user experience.

## Requirements

* 6+ years experience specializing in web and user experience design.
* A professional portfolio demonstrating a robust background in web, UX, and visual design solutions across devices and media channels (candidates without a portfolio will not be considered).
* Experience guiding user experience research and design initiatives.
* Thorough knowledge of digital design fundamentals, mobile and responsive design, and web development best practices.
* Knowledge of HTML, CSS, and JavaScript (jQuery).
* Robust knowledge of Adobe CC and prototyping tools (e.g. Figma, Sketch, etc.)
* Experience with conversion design, running A/B tests and funnel optimization.
* You use research and best practices to create, validate, and present your ideas to project stakeholders.
* Obsessive over the details and creating thoughtful and intuitive experiences.
* Able to iterate quickly and embrace feedback from many perspectives.
* Substantial understanding of information architecture, interaction design, and user-centered design best practices.
* Understanding of Git and comfortable using the command line.
* A track record of being self-motivated and delivering on time.
* Candidates in Americas timezones will be preferred.
* Ability to use GitLab

#### Nice-to-haves

* Proficiency in HTML, CSS and JavaScript (JQuery).
* Able to pitch in on other design efforts; i.e. branding, illustration, swag, social ad campaigns, digital publications, etc.
* Familiarity with SaaS marketing a plus.
* Familiarity with GitLab or similar software a plus.
* Experience working in a fully or partially remote company.
* Agile experience a plus.
* Experience with vendor management a plus.
* Design System experience a plus.
* Motion graphics experience a plus.
* Familiarity with Federal Usability Standards and 508 Accessibility Compliance a plus. 
* Basic Ruby knowledge; our website is built on Middleman using HAML.


## Levels


### Senior Digital Experience Designer/Developer


Roles and responsibilities for this position include everything in the Digital Experience Designer/Developer role, plus:

#### Responsibilities
* Guide major research, design and development initiatives on about.gitlab.com and across omni-channel funnels.
* Work across disciplines and internal tiers to align initiatives and stakeholders to solutions.
* Oversee the development of personas and user flows to ensure audience-appropriate solutions and seamless, conversion-driven buyer journeys.
* Be a primary contributor to brand and design style guides, design systems, and front-end framework(s) to drive efficiency and consistency in our brand and user experience.
* Mentor internal GitLab team members and vendors in the design, development and delivery of digital media solutions.
* Facilitate ideation sessions and iterative, co-design sessions with stakeholders.
* Persuasively present design solutions to cross-functional internal audiences to generate excitement and consensus.


#### Requirements
* 8+ years of experience working on digital design products.
* 3+ years of experience in either a project leadership or supervisory role.
* Exemplary relationship-building and open communication skills to listen, provide feedback, inspire and mentor team members.
* Exceptional attention to detail and organizational skills.
* Experience developing and ensuring the proper application of brand standards and guidelines.
* Genuine interest in the latest design trends and technological developments in the industry.
* Shared interest in our values, and working in accordance with those values. 
* Desire to thrive in an environment where self-learning and self-service is encouraged and instilled as a part of our culture.

#### Nice-to-haves

* Experience as agile project manager a plus.


### Senior Manager, Digital Experience 

In this role, you will work with internal and external stakeholers and contributors to design, deliver and optimize omni-channel experiences end-to-end with measurable results. You will work closely with the Director of Brand and Digital Design, Brand Manager, Product and Marketing leads, and third-party vendors to synthesize a wide spectrum of strategies and solutions, such as design systems, websites and mico-sites, conversion funnels, and email drip campaigns. 

Roles and responsibilities for this position include everything in the Senior Digital Experience Designer/Developer role, plus:

#### Responsibilities
* Grow Gitlab's digital experience team, hiring and mentoring the team to ensure its continued success.
* Foster a culture of innovation and continual improvement aligned to GitLabs mission and CREDIT values.
* Work with Brand and Digital and Marketing to identify, prioritize and implement value-driven MVCs. 
* Mentor in the development of brand and design style guides, design systems and front-end frameworks.
* Enable customer-centric solutions by serving as a customer advocated and emplyoing tools such as buyer personas, user flows, and feedback loops. 
* Conduct heuristic research to idnetify ways to improve GitLabs brand and digital experiences.  


#### Requirements

* 12+ years of experience working on digital design products.
* 6+ years of experience in either a project leadership or supervisory role.

#### Nice-to-haves

* Experience as agile project manager a plus.


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Portfolios of qualified candidates will be reviewed by our hiring team
* Select candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* First, candidates will be invited to meet with a member of the Brand and Digital team
* Second, candidates may be invited to complete a design/development assessment
* Third, candidates will be invited to meet wwith a member of the Brand and Digital team
* Fourth, candidates will be invited to meet with our Director of Brand and Digital
* Successful candidates may be invited to schedule an interview with our CMO
* Finally, successful candidates may be asked to interview with our CEO
