---
layout: job_family_page
title: Corporate Communications Manager
---

## Corporate Communications Manager

Corporate Communications managers at GitLab have a unique opportunity to contribute to our [mission](/company/strategy/#mission). Through your leadership, experience, and insight, you will position GitLab as a trustworthy partner to customers with a strong track record of addressing business challenges, increase overall brand awareness, provide air cover for business as a whole, and come up with thought leadership opportunities. This position will work across teams and the globe to develop and execute integrated communications campaigns in line with GitLab initiatives. You will be expected to prioritize and manage your work with minimal guidance from leadership or other corporate communications team members.

We work in a unique way at GitLab, where flexibility and independence meet a high paced, pragmatic way of working. And everything we do is in the open. We recommend looking at our [corporate marketing handbook](/handbook/marketing/corporate-marketing/) to get started.

## Job Grade

The Corporate Communications Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).


## Responsibilities
* Execute integrated communications strategy and vision.
* Manage GitLab’s PR agency relationships and ensure their programs are in line with overall corporate marketing objectives and goals.
* Work closely with others in the Corp Comms department to develop messaging, press releases, contributed articles, event strategy and media relations strategy for GitLab announcements and corporate marketing campaigns.
* Operationalize customer use cases and problems that GitLab solves.
* Develop overarching company storylines for media and determine overall public relations calendar.
* Work closely with executives communications leed to oversee entire executive visibility program; including but not limited to key message creation, media relationship building, media tours, contributed articles, and speaking presence.
* Work closely with Vice President of Investor Relations on milestones/roadmap for communication portion of becoming a public company.
* Strategically lead crisis communications efforts.
* Conduct media training for spokespeople.
* Responsible for ideation of Corp Communications activities, OKRs, and reporting on results.
* Measure our department successes in relation to awareness and impact.
* Independently manage projects from start to finish.

## Requirements
* 7+ years experience in public relations and/or marketing communications.
* Experience with crisis communications, executive visibility and investor relations.
* PR agency experience.
* Experience in enterprise software or developer public relations.
* Proven experience in working with  agencies and freelancers.
* Strong media relations skills and experience working with analysts.
* A natural storyteller with excellent writing skills.
* Creative, thoughtful and passionate about storytelling.
* Able to coordinate across many teams and perform in a fast-moving startup environment.
* Proven ability to be self-directed and work with minimal supervision.
* Outstanding written and verbal communications skills.
* You share our values, and work in accordance with those values.
* Highly organized, detail-oriented and able to meet deadlines consistently.
* Ability to use GitLab

## Levels

### Director, Corporate Communications

### Job Grade
The Director of Corporate Communications is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). The Director of Corporate Communications leads the overall corporate communications team, public relations team and social media team.

### Requirements
* 10+ years experience managing public relations and communications teams
* Experrience developing overall strategy for corporrate communications, public relations and social media teams.
* Experience managing budgets aligning with communications objectives and goals.
* Experience working with leaderrship to drive external awareness activities.

### Senior Corporate Communications Manager

#### Job Grade

The Senior Corporate Communications Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Requirements
* 10+ years experience in public relations and/or marketing communications
* Experience in executive visibility/communications
* Experience running public relations/communications efforts at an enterprise technology company.
* Experience managing  agencies and driving results.
* Experience hiring and managing a team of communications professionals.

## Specialties

### Executive Communications
The executive communications manager will work closely with the executive leadership team to develop content for executive speaking opportunities, extend the reach of executive participation in external events, develop a platform for “Pick Your Brain” or other Zoom series, executive contributed articles and more.

#### Responsibilities
* Work closely with the executive leadership team to develop strong narrative presentations and content for external speaking opportunities.
* Work with the brand, design, technical marketing team and others needed to develop state of the art slide decks for external events.
* Lead speaking coaching sessions with leadership team to make sure they comfortably deliver the content in a natural and charismatic way; incorporate edits as needed.
* Think strategically about other executive leadership platforms and how we should be promoting them externally, whether that is via our blog, contributed articles, podcasts, YouTube or other external platforms.
* Work closely with others in the Corporate Communications department and with PR agencies to share presentation narratives for media consumption and obtain media messages/interview feedback to incorporate in external speaking presentations.
* Work closely with Senior Manager, Corporate Communications to manage executive visibility programs.
* Manage any outside agencies or freelancers hired to assist with the executive visibility program.
* Work closely with Executive Assistants to plan executive speaking schedules for the year.

#### Requirements
* 5+ years experience in an executive communications role.
* Experience running public relations, communications or marketing efforts at an enterprise technology company.
* Experience in enterprise software and/or open source.
* Creative, thoughtful and passionate about storytelling.
* Outstanding written and verbal communications skills.
* Experience with presentation building - both narrative flow and design
* Able to coordinate across many teams and perform in a fast-moving startup environment.
* Proven ability to be self-directed and work with minimal supervision.
* You share our values, and work in accordance with those values.
* Highly organized, detail-oriented and able to meet deadlines consistently.

### PR
The GitLab [PR Manager](/job-families/marketing/pr-manager/) will help GitLab build thought leadership and drive conversation in external communications channels. This position is responsible for developing and driving thought leadership content, securing media coverage for product, partner, channel, community and company announcements and stories, assisting with crisis and rapid response requests, implementing proactive PR campaigns and company award submissions.

### Social Marketing
The [Social Marketing Manager](/job-families/marketing/social-marketing-manager/) is a key part of building GitLab’s brand into one that is loved by its audience of developers, IT ops practitioners, and IT leaders.

## Performance Indicators
* Increase overall brand awareness through key message pull through in media articles and speaking presenations, secure opportunities for executives to speak at conferences and with media, submit strong award applications.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a screening call with one of our Global Recruiters.
* Candidates then may be offered a 30 minute interview with our Senior Manager, Corporate Communications.
* Next, candidates will be invited to schedule an interview with the Senior Director, Corporate Marketing.
* Candidates will then be invited to schedule interviews with key team members from relevant company departments that the role will work with closely.
* Next, candidates will be invited to schedule an interview with the Chief Marketing Officer.
* For Executive roles, candidates may be invited to put together a presentation for our Executive team.
* Finally, our CEO and other executives may choose to conduct a final interview.
* Successful candidates will subsequently be made an offer via video, phone, or email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
