---
layout: job_family_page
title: Demand Generation Marketing
---

## VP of Demand Generation

The VP of Demand Generation is responsible for overseeing all aspects of demand generation to achieve revenue goals that support the company’s fast-paced growth. This role is a highly cross-functional, strategic role which will require both creative and analytical thinking. They work closely with leaders across Revenue Marketing, Product Marketing, Product Management, and Sales. The VP of Demand Generation is a marketing team leader, responsible for the creation and execution of GitLab’s global marketing campaigns to generate leads and new business pipeline. The campaigns must consider global revenue goals and be multi-channel, able to be executed through digital and traditional tactics in partnership with Revenue Marketing. The campaigns must also be measurable and tied to GitLab’s global revenue goals. The responsibilities of the role include campaign strategy development, program execution, team management and process optimization. The position requires a driven, innovative and energetic demand generation leader, with a talent for empowering high performing teams and an ability to get things done. 

### Job Grade

The VP of Demand Generation is a [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
* Develop the company's demand generation campaign strategy to drive new business, as well as expand the existing customer base. Own execution of its associated plan, including scope, target outcomes, key metrics and measures, and required inputs and resources to deliver
* Work closely with Revenue Marketing and sales to deploy campaigns and ensure alignment across teams
* Build systems and processes that will track and report on key performance metrics and use data to optimize campaign strategies continually
* Work collaboratively to build, inspire, manage and grow a high-performing global team
* Continuously evaluate the performance and ROI of campaigns, adjusting demand tactics and strategy accordingly
* Work closely with product marketing, marketing operations, and sales to ensure message optimization and cross-functional alignment

### Requirements
* 10-15+ years experience in roles of increasing responsibility, managing $100MM-$1B in revenue pipeline in high-growth companies; current or previous VP title, reporting to a CMO preferred
* Experience running a global team of at least dozens of people with multiple levels of management
* Must have a firm grasp of digital marketing best practices, yet have an innate curiosity to continually investigate new trends
* Experience with digital and non-digital demand gen tactics 
* Familiarity with open-source, cloud native, cloud, and Kubernetes a plus
* Excellent strategic thinking and ability to influence and work collaboratively across organizations
* Fluency in demand generation metrics and approaches to test and optimize them including the use of Marketo and Salesforce
* Proven experience scaling demand generation programs through technology, data and process.
* Demonstrated success and comfort driving global programs in multiple languages
* An innate sense of urgency to drive programs forward
* Ability to use GitLab

### Performance Indicators
* [Net new business pipeline created ($'s) v plan > 1](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions)
* [Total number of  Marketo Qualified Leads by month](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions)

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Candidates will then meet with the Senior Director of Revenue Marketing.
* Candidates will then be asked to schedule an interview with the Manager of Field Marketing, Americas.
* Candidates will then meet with the CMO.
* Following successful completion of the previous steps, final candidates will then be asked to meet with a panel of Sales Development Managers.
* Final candidates may be asked to complete an assessment, and meet with the Senior Director of Revenue Marketing again.
* Successful candidates will subsequently be made an offer via video or phone.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
