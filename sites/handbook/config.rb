monorepo_root = File.expand_path('../..', __dir__)

#--------------------------------------
# Monorepo-related configuration
#--------------------------------------

# hack around relative requires elsewhere in the shared code by adding the monorepo root to the load path
$LOAD_PATH.unshift(monorepo_root)

set :data_dir, "#{monorepo_root}/data"
set :helpers_dir, "../../helpers" # This has to be relative, because Middleman's ExternalHelpers#after_configuration uses File.join(app.root, ...)

require_relative '../../extensions/monorepo.rb'
activate :monorepo do |monorepo|
  monorepo.site = 'handbook'
end

#--------------------------------------
# End of Monorepo-related configuration
#--------------------------------------

#----------------------------------------------------
# Config which is duplicated from top-level
#----------------------------------------------------

require_relative "../../extensions/breadcrumbs"
require_relative "../../extensions/codeowners"
require_relative '../../extensions/only_debugged_resources'
require_relative "../../lib/homepage"
require 'lib/mermaid'
require 'lib/plantuml'

# Settings
set :haml, { format: :xhtml }
set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Paths with custom per-page overrides
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# Don't render or include the following into the sitemap
ignore '**/.gitkeep'

# Extensions
activate :syntax, line_numbers: false
activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end
activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false
activate :codeowners
if ENV['MIDDLEMAN_DEBUG_RESOURCE_REGEX']
  ::Middleman::Extensions.register(:only_debugged_resources, OnlyDebuggedResources)
  activate :only_debugged_resources
end

# Build-specific configuration
configure :build do
  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup

  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true
end

#----------------------------------------------------
# End of config which is duplicated from top-level
#-----------------------------------------------------

#---------------------------------------
# Config which is specific to this site
#---------------------------------------

config_proxy_resources = File.join(Middleman::Application.root, 'config_proxy_resources.rb')
instance_eval(File.read(config_proxy_resources), config_proxy_resources, 1)

#---------------------------------------------
# End of config which is specific to this site
#---------------------------------------------
