---
layout: handbook-page-toc
title: GitLab the Product
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**Principles**](/handbook/product/product-principles) - [**Processes**](/handbook/product/product-processes) - [**Categorization**](/handbook/product/product-categories) - [**GitLab the Product**](/handbook/product/gitlab-the-product) - [**Being a PM**](/handbook/product/product-manager-role) - [**Performance Indicators**](/handbook/product/performance-indicators/) - [**Leadership**](/handbook/product/product-leadership/)

## GitLab the Product

### Single application

We believe that a single application for the DevOps lifecycle
based on [convention over configuration](/handbook/product/product-principles/#convention-over-configuration) offers a superior user experience. The
advantage can be quoted from the [Wikipedia page for convention over](https://en.wikipedia.org/wiki/Convention_over_configuration)
configuration:
"decrease the number of decisions that developers need to make, gaining
simplicity, and not necessarily losing flexibility". In GitLab you only have to
specify unconventional aspects of your workflow. The happy path is
**frictionless from planning to monitoring**.

We're doubling down on our product for concurrent DevOps which brings the entire
lifecycle into one application and lets everyone contribute. We are leaning into
what our customers have told us they love: our single application strategy, our
pace of iteration, and our deep focus on users.

Consider opportunities to take advantage of this unique attribute in early
iterations. Integrating features with different parts of the application can
increase the adoption of early iterations. Other advantages:

- A minimal viable feature that is well integrated can be more useful than a
sophisticated feature that is not integrated.

Although not every feature needs to be integrated with other parts of the
application, you should consider if there are unique or powerful benefits for
integrating the feature more deeply in the second or third iteration.

### GitLab.com

GitLab.com runs GitLab Enterprise Edition.

To keep our code easy to maintain and to make sure everyone reaps the benefits
of all our efforts, we will not separate GitLab.com codebase from the Enterprise Edition codebase.

To avoid complexity, [GitLab.com tiers and GitLab self-managed tiers](/handbook/marketing/product-marketing/tiers/) strive to match 1:1.

- Free: Core features
- Bronze: Starter features
- Silver: Premium features
- Gold: Ultimate features
- Public projects are considered to have a Gold subscription level

Since we are not able to give admin access and do not yet have full feature
parity between self-managed instances and GitLab.com, we avoid saying that there is
a one to one match between subscription levels and tiers in marketing materials.
This has been a source of confusion in the past for customers.

#### GitLab.com subscription scope and tiers

GitLab.com subscriptions work on a namespace basis, which can mean:

- personal namespace, e.g. `JaneDoe/*`
- group namespace, e.g. `gitlab-org/`

This means that group-level features are only available on the group namespace.

Public projects get Gold for free.
Public groups _do not_ get Gold for free. Because:

- This would add significant additional complexity to the way we structure our features, licenses and groups. We don't want to discourage public groups, yet it wouldn't be fair to have a public group with only private projects and for that group to still get all the benefits of Gold. That would make buying a license for GitLab.com almost entirely moot.
- All value of group level features is aimed at organisations, i.e. managers and up (see our [stewardship](/company/stewardship/)). The aim with giving all features away for free is to enable and encourage open source projects. The benefit of group-level features for open source projects is significantly diminished, therefore.
- Alternative solutions are hard to understand, and hard to maintain.

Admittedly, this is complex and can be confusing for product managers when implementing features.
Ideas to simplify this are welcome (but note that making personal namespaces equal to groups is not one of
them, as that introduces other issues).

### Paid Tiers

Keep in mind that the [CEO is the DRI for pricing and tiers](/handbook/ceo/pricing/#departments). This includes any changes that directly impacts how we charge customers under our licensing model, such as a [change to how we handle active users](https://gitlab.com/gitlab-org/gitlab/issues/22257). Please review the entirety of the [stewardship](/company/stewardship/) and [pricing](/handbook/ceo/pricing/) pages before making any determinations of which tier a given feature should go in.

#### What goes in what paid tier

Our [stewardship principles](/company/stewardship/) determine whether something belongs in a
paid tier. The [likely buyer](/handbook/ceo/pricing/#the-likely-type-of-buyer-determines-what-features-go-in-what-tier) determines which tier.

#### Determining the tier of a new feature

When making a decision on the tier for a new feature, please refer to the [pricing page](/handbook/ceo/pricing/) for guidance. Be sure to consider
documenting your rationale for the decision in the issue description, including a reference to our [stewardship page](/company/stewardship/) when appropriate.

Please indicate the applicable tier for an issue by applying the label associated with the tier (e.g. `GitLab Core`, `GitLab Starter`) and the
`Enterprise Edition` label for features targeting a paid tier. Ensure this is defined before feature development begins.

Should you have any questions when making this decision, do not hesitate to collaborate with your manager, [product leadership](/handbook/product/product-leadership/), or the CEO in the issue for clarification.

#### Bringing features to lower tiers and pricing changes

To propose bringing a feature to a lower tier or making a change that impacts how we charge customers, please follow the process on the [CEO pricing page](/handbook/ceo/pricing/#bringing-features-to-lower-tiers-and-pricing-changes).

#### Paid Tier requirements

All Starter, Premium, and Ultimate features must:

- Work easily for our customers that self-host GitLab. i.e. Their
licenses need not be updated and the new feature is default-on for the
instance.
- Work with GitLab.com Bronze / Silver / Gold subscriptions. This means there has to be
some way of toggling or using the feature at a namespace level.
- Have documentation.
- Be featured on [products](/product/) and [DevOps tools](/devops-tools) at launch.

### Alpha, Beta, GA

Occasionally we need to test large, complex features before we are confident
that we'll be able to scale, support and maintain them as they are.
In this case we have the option to release them as Alpha or Beta versions.

In general, we should avoid releasing Alpha or Beta versions of features.
A minimal viable change should be _viable_ and therefore should not need a
pre-release. That said, if there is a situation where we have no valid alternative,
the definitions of each stage is below.

It's never acceptable to make changes that risk any damage to existing production
data accessed by our users.

#### Alpha

- not ready for production use
- unstable and can cause performance and stability issues
- the configuration and dependencies are likely to change
- features and functions may be removed
- data loss can occur (be that through bugs or updates)

#### Closed Beta

Similar to Beta, but only available to selected users.

- not ready for production use
- unstable and can cause performance and stability issues
- configuration and dependencies unlikely to change
- features and functions unlikely to change
- data loss less likely

#### Beta

- not ready for production use
- unstable and can cause performance and stability issues
- configuration and dependencies unlikely to change
- features and functions unlikely to change
- data loss not likely

#### Generally Available (GA)

Passed the [Production Readiness Review](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md) for GitLab.com, which means that it is:

- ready for production use at any scale
- fully documented and supported

### Deprecating and removing features

Deprecating features follows a particular pattern.
Use the language `Deprecated` or `Removed` to specify the state
of a feature that is going to be or is removed.

Features that are deprecated or removed should be:

1. Labelled accordingly in the documentation
1. Labelled accordingly in the application
1. Removed from marketing pages

#### Deprecated

- May be removed at a specified point in the future
- Still available with new installations, but documentation mentions deprecated state
- No longer receives new features and active development, but still supported: critical bugs and regressions are fixed

#### Removed

- No longer available in the latest version of the product

### Naming features

Naming new features or [renaming existing features](https://docs.gitlab.com/ee/development/renaming_features.html) is notoriously hard and sensitive to many opinions.

#### Factors in picking a name

- It should clearly express what the feature is, in order to avoid the [AWS naming situation](https://www.expeditedssl.com/aws-in-plain-english).
- It should follow [usability heuristics](http://www.designprinciplesftw.com/collections/10-usability-heuristics-for-user-interface-design) when in doubt.
- It should be common in the industry.
- It should not overlap with any other existing concepts in GitLab.
- It should have as few words as possible (so people won't use a shortened name).
- If you remove words from the name, it is still unique (helps to give it as few words as possible).

#### Process

- It's highly recommended to start discussing this as early as possible.
- Seek a broad range of opinions and consider the arguments carefully.
- The PM responsible for the area involved should make the final decision and not delay the naming.
- Naming should definitely not be a blocker for a feature being released.
- Reaching consensus is not the goal and not a requirement for establishing a name.

#### Renaming

The bar for renaming existing features is extremely high, especially for long-time features with a lot of usage.
Some valid but not exclusive reasons are:

- New branding opportunities
- Reducing confusion as we introduce new adjacent features
- Reducing confusion as we re-factor existing features

### Permissions in GitLab

Use this section as guidance for using existing features and developing new ones.

1. Guests are not active contributors in private projects. They can only see, and leave comments and issues.
1. Reporters are read-only contributors: they can't write to the repository, but can on issues.
1. Developers are direct contributors, and have access to everything to go from idea to production,
unless something has been explicitly restricted (e.g. through branch protection).
1. Maintainers are super-developers: they are able to push to master, deploy to production.
This role is often held by maintainers and engineering managers.
1. Admin-only features can only be found in `/admin`. Outside of that, admins are the same as the highest possible permission (owner).
1. Owners are essentially group-admins. They can give access to groups and have
destructive capabilities.

To keep the permissions system clear and consistent we should improve our roles to match common flows
instead of introducing more and more permission configurations on each resource, if at all possible.

For big instances with many users, having one role for creating projects, doing code review and managing teams may be insufficient.
So, in the long term, we want our permission system to explicitly cover the next roles:

1. An owner. A role with destructive and workflow enforcing permissions.
1. A manager. A role to keep a project running, add new team members etc.
1. A higher development role to do code review, approve contributions, and other development related tasks.

All the above can be achieved by iteratively improving existing roles and maybe [adding one more](https://gitlab.com/gitlab-org/gitlab/issues/21906).

[Documentation on permissions](https://docs.gitlab.com/ee/user/permissions.html)

### Security Paradigm

You can now find our [security paradigm](/direction/secure/#security-paradigm) on the [Secure Strategy](/direction/secure/) page.

Also see our [Secure Team engineering handbook](/handbook/engineering/development/secure/).

### Statistics and performance data

Traditionally, applications only reveal valuable information about usage and
performance to administrators. However, most GitLab instances only have a handful of
admins and they might not sign in very often. This means interesting data is rarely
seen, even though it can help to motivate teams to learn from other teams,
identify issues or simply make people in the organisation aware of adoption.

To this end, performance data and usage statistics should be available to all users
by default. It's equally important that this can be optionally restricted to admins-only,
as laws in some countries require this, such as Germany.

### Internationalisation

GitLab is developed in English, but supports the contribution of other
languages.

GitLab will always default to English. We will not infer the language /
location / nationality of the user and change the language based on that.
You can't safely infer user preferences from their system settings either.
Technical users are used to this, usually writing software in English,
even though their language in the office is different.

### Performance

Fast applications are better applications. Everything from the core user experience,
to building integrations and using the API is better if every query is quick, and
every page loads fast. When you're building new features, performance has to be top of mind.

We must strive to make every single page fast. That means it's not acceptable for new
pages to add to performance debt. When they ship, they should be fast.

You must account for all cases, from someone with a single object, to thousands of objects.

Read the handbook page relating to [performance of GitLab.com](/handbook/engineering/performance), and note the Speed Index target shown there
(read it thoroughly if you need a detailed overview of performance). Then:

- Make sure that new pages and interactions meet the Speed Index target.
- Existing pages should never be significantly slowed down by the introduction of new features
or changes.
- Pages that load very slowly (even if only under certain conditions) should be sped up by
prioritizing work on their performance, or changes that would lead to improved page load speeds
(such as pagination, showing less data, etc).
- Any page that takes more than 4 seconds to load (speed index) should be considered too slow.
- Use the [availability & performance priority labels](/handbook/engineering/performance/#performance-labels)
to communicate and prioritize issues relating to performance.

You must prioritize improvements according to their impact (per the [availability & performance priority labels](/handbook/engineering/performance/#performance-labels)).
Pages that are visited often should be prioritized over pages that rarely have any visitors.
However, if page load time approaches 4 seconds or more, they are considered no longer
usable and should be fixed at the earliest opportunity.

#### Restriction of closed source Javascript

In addition, to meet the [ethical criteria of GNU](https://www.gnu.org/software/repo-criteria-evaluation.html),
all our javascript code on GitLab.com has to be free as in freedom.
Read more about this on [GNU's website](https://www.gnu.org/philosophy/javascript-trap.html).

### Why cycle time is important

The ability to monitor, visualize and improve upon cycle time (or: time to
value) is fundamental to GitLab's product. A shorter cycle time will allow you
to:

- respond to changing needs faster (i.e. skate to where the puck is going to be)
- ship smaller changes
- manage regressions, rollbacks, bugs better, because you're shipping smaller changes
- make more accurate predictions
- focus on improving customer experience, because you're able to respond to their needs faster

When we're adding new capabilities to GitLab, we tend to focus on things that
will reduce the cycle time for our customers. This is why we choose
[convention over configuration](/handbook/product/product-principles/#convention-over-configuration)
and why we focus on automating the entire software development lifecycle.

All friction of setting up a new project and building the pipeline of tools you
need to ship any kind of software should disappear when using GitLab.

### Plays well with others

We understand that not everyone will use GitLab for everything all the time,
especially when first adopting GitLab. We want you to use more of GitLab because
you love that part of GitLab. GitLab plays well with others, even when you use
only one part of GitLab it should be a great experience.

GitLab ships with built-in integrations to many popular applications. We aspire
to have the world's best integrations for Slack, JIRA, and Jenkins.

Many other applications [integrate with GitLab](/partners/integrate/), and we are open to adding new integrations to our [technology partners page](/partners/). New integrations with GitLab can vary in richness and complexity; from a simple webhook, and all the way to a [Project Service](https://docs.gitlab.com/ee/user/project/integrations/project_services.html).

GitLab [welcomes and supports new integrations](/partners/integrate/) to be created to extend collaborations with other products.
GitLab plays well with others by providing APIs for nearly anything you can do within GitLab.
GitLab can be a [provider of authentication](https://docs.gitlab.com/ee/integration/oauth_provider.html) for external applications.

There is some natural tension between GitLab being a single-application for the
entire DevOps lifecycle, and our support for better user experience via integration
with existing DevOps tools. We'll prioritize first our efforts to improve the single-application
experience, second to enable a rich ecosystem of partners, and third to improve integration with the broader ecosystem
to other tools. GitLab is open-source so this should not prohibit contributors adding
integrations for anything that they are missing - as long as it fits with GitLab product vision.

If you don't have time to contribute and are a customer we'll gladly work with you to design the API addition or integration you need.

### Who GitLab competes with

In pursuit of our product vision of becoming a complete platform for the entire DevOps lifecycle, delivered as a single application, GitLab will inevitably compete for user and business with other DevOps tools technologies and vendors. For clarity, GitLab publicly competes with other vendors only in categories where our product is lovable (in our [maturity model](https://about.gitlab.com/direction/maturity/)). We may at times name these competitors publicly.

For all other categories, GitLab will highlight examples of tools that have functionally similar features to the features GitLab is developing, but we will not name them as explicit competitors. This does not mean that we will not be trying to acquire users or business based on those categories, but it does mean that we will not refer to those other vendors as 'competitors', as our goal is to pursue our product vision (not to specifically win against other tools or vendors where we are not yet lovable).