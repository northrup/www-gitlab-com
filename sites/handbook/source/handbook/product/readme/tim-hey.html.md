---
layout: markdown_page
title: "Tim Hey's README"
---

# Tim Hey's — README.md

I created this personal README document to help you get to know me and how I work. This document is inspired by Matej’s [README](https://gitlab.com/matejlatin/focus#matej-readmemd) and the [Product team README's](https://about.gitlab.com/handbook/product/readme/#product-readmes).

- [GitLab Profile](https://gitlab.com/timhey)
- [Team Page](https://about.gitlab.com/company/team/#timhey)
- [Current OKRs](https://gitlab.com/timhey/focus/blob/master/OKRs/current.md)
- [Current Tasks](https://gitlab.com/timhey/focus/blob/master/Tasks/Current.md)

## About me

- My name is Tim Hey (last name jokes are welcome) I am a Principal Product Manager on the [Growth](https://about.gitlab.com/handbook/product/growth/) team at GitLab and I focus on [Expansion](https://about.gitlab.com/direction/expansion/).
- I currently live in [New Boston, New Hampshire](https://www.google.com/maps/place/New+Boston,+NH+03070/@42.9777511,-71.7556169,12z/data=!3m1!4b1!4m5!3m4!1s0x89e233e8a9946e0d:0x590e6c13b1743f65!8m2!3d42.9761945!4d-71.6939626) 🇺🇸 with my wife Elizabeth, our 2 sons and our 2 dogs (they are rescues and we have no clue what breed they are).
- I was raised in New Hampshire but lived on [Hilton Head Island](https://www.google.com/maps/place/Hilton+Head+Island,+SC/@32.1834696,-80.8174359,12z/data=!3m1!4b1!4m5!3m4!1s0x88fc79dc8ed319ad:0x2ce5a67aeba2283d!8m2!3d32.216316!4d-80.752608) for 8 years, this is where my wife and I started our life together and we plan to move back when the children are older.
- I’m an [introverted extrovert](https://en.wikipedia.org/wiki/Extraversion_and_introversion) which for me means that I enjoy spending time with others but I recharge in solitude. You will know which mode I’m in within seconds of interacting with me.
- I studied business management and sustainable product design and development at Keene State College in New Hampshire and it just so happens that it’s exactly what I’ve built a career around.
- I ride bikes 🚴‍♂️and recently fell in love with gravel. If I’m feeling fit I will compete in local gravel races, criteriums or cyclo-cross races. When I do compete I race for team [Mad Alchemy](https://www.madalchemy.com/). Feel free to follow me on [Strava](https://www.strava.com/athletes/2610245)
- I’m a podcast junkie, some of my favorites are [TrainerRoad](https://www.trainerroad.com/podcast), [a16z](https://a16z.com/podcasts/), [Breakout Growth](https://www.seanellis.me/the-breakout-growth-podcast.html) and [Rich Roll](https://www.richroll.com/)
- I recently took the Clifton Strengths assessment If you are curious you can learn more [here](https://gitlab.com/gitlab-org/growth/product/issues/797)
- Lastly, for more than a decade now I've lived a life in recovery and I'm very open about it, I won't lead with it but if you want to talk don't hesitate to reach out.

## How I work

- My timezone is [EST](https://time.is/ET)
- My working hours are 9am to 5:30pm with a 1/2-hour lunch break at 12pm.
- I prefer fixed working hours because it allows me to be more productive and efficient.
- I block out 3-4 hours each day (in the morning) for meaningful work.

## My typical week

- I use Monday's to advance deliverables, catch up with the team and address any blockers that may have popped up.
- I have most of my meetings Tuesday - Thursday and like to keep Fridays meetings-free.
- Each Friday I prepare a plan for the next week.

## My typical day

- I get up at 6AM on most days (if my children don’t wake me up first).
- I workout 4-5 times per week and try to do this between 7-8:30 AM.
- I start working at 9AM and take my first break around 12PM.
- I check my email in the morning around 9 and after lunch at 1pm.
- I prefer to have meetings in the afternoon hours but will meet anytime during my normal working hours.

## What’s keeping me busy

- I have my own OKRs and try to keep a good mix of personal and work related objectives to keep me on track. Here are [my current OKRs](https://gitlab.com/timhey/focus/blob/master/OKRs/current.md).
- I also create a weekly plan every friday, this helps end the week and hit the ground running after the weekend. To see what I'm currently working on check [my current tasks](https://gitlab.com/timhey/focus/blob/master/Tasks/Current.md).

## How to reach me

- **Slack**: Direct message or mention is probably the best way to reach me during the day (my handle is @tim_hey). I will always have Slack open during business hours but I have notifications disabled on many of the channels (we have so many channles) so it may take some time before I respond. If you have an urgent message or need me to repond quickly the best approach is a direct message with a link to the issue you need me to contribute to.
- <b>[E-mail](mailto:they@gitlab.com)</b>: I read my emails twice a day and work through them like a task list. If you have an urgent email please use [URGENT]: in your subject line so I see it. Please do not feel obligated to read or respond to any of the emails you receive from me outside of your normal hours.
- I like to focus deeply on the task at hand, so I turn off notifications for email and Slack. Please expect my responses on email and Slack to be asynchronous. Your messages are important to me and I will respond as soon as I can.
- You can schedule a 30 min coffee chat with me via <b>[Calendly](https://calendly.com/timhey-gitlab)</b>.