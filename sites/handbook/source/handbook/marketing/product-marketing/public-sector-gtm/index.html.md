---
layout: markdown_page
title: "Public Sector Go To Market"
---

### Messaging

#### Booth Messaging / Tagline

* **Accelerate Speed to Mission.**
*Deliver faster, better, more secure code.*

<br>



#### Market-Resonating Themes & Use Case Positioning

*Note: The following themes and use cases should be viewed as sub-messages to the overall government imperatives of Digital Tranformation and IT Modernization.*
<br>

* **Speed & Efficiency**
    - Continuous Integration, Continuous Delivery, Continuous Security (*Use Cases: CI, CD*)
    - Shorter time-to-value (*Use Case: CI*)
    - Automating pipelines (*Use Case: CD*)
    - Empowering the workforce / shift to higher-level work (*Use Case: End-to-End DevOps*)
* **Security**
    - Proactive security integration (*Use Case: DevSecOps*)
    - Data / code integrity (*Use Cases: SCM, DevSecOps*)
    - Trust / confidence in pipelines (*Use Cases: CD, DevSecOps, GitOps*)
    - Disciplined data / visibility (*Use Cases: SCM, CI, End-to-End DevOps*)
* **Team Collaboration** (*Use Case: End-to-End DevOps*)
* **Innovation**
    - Cultivating innovation throughout the ecosystem (*Use Cases: SCM, End-to-End DevOps*)
    - Open Source -> innersource -> INNOVATION (*Use Cases: SCM, Agile planning, End-to-End DevOps, Cloud Native*)
    - Pre-empting disruption with pilots / prototypes / iteration (*Use Cases: SCM, Agile planning, End-to-End DevOps*)
* **Citizen Experience (CX) / Human-Centered Design** (*Use Cases: Agile planning, End-to-End DevOps*)

<br>



### Topic Focus for PubSec Segments

* **Defense & Intell Community:** "Speed to Mission" and hardened containers / CAC authentication / Security controls / Low-to-High import/export
* **Civilian:** “DevSecOps”, “IT Modernization”, and “Digital Transformation”
* **State & Local:**  “DevSecOps”, “IT Modernization”, and “Digital Transformation”
* **Education:**  “DevSecOps”, "Privacy", and "Security"

<br>

### [SWOTT Analysis](https://docs.google.com/presentation/d/1yVnIZisAmXOqJMnCF5UyTSD2PcD8YfmuJ-LmABbVsaM/edit?usp=sharing)


<br>

### Personas (WIP: Public Sector-specific roles will be added)

*Note: Within government, titles are inconsistent, so assess for role / function instead of title when engaging with customers.* 

* [General GitLab Roles and User & Buyer Personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/)



### Content 
Ungated asset links can be found on [this document](https://docs.google.com/document/d/1Yvqq-X2fcUgwSKC-UtIP7qLnywIs9BA65F5NgyR4PoQ/edit?usp=sharing). 

* [GitLab Public Sector Capabilities Statement](https://about.gitlab.com/images/press/gitlab-capabilities-statement.pdf)  
* [Speed to Mission (whitepaper)](https://about.gitlab.com/resources/whitepaper-speed-to-mission/)
* [The Hidden Costs of DevOps Toolchains (with guest speaker from Forrester)](https://about.gitlab.com/webcast/simplify-to-accelerate/)
* [DevOps Powering Your Speed to Mission (webcast)](https://about.gitlab.com/webcast/devops-speed-to-mission/)
* [Mission Mobility: A DevSecOps Discussion (webcast)](https://about.gitlab.com/webcast/mission-mobility/)
* [Government Matters Technology Leaders Innovation Series: Low-to-High Collaboration (with Marc Kriz) (video)](https://youtu.be/RvZSTCbPkiE)
* [Modernizing Government IT through DevOps (whitepaper)](https://about.gitlab.com/resources/whitepaper-modernizing-government-it/)
* [Cross Domain DevSecOps: Low-to-High Side Collaboration](https://drive.google.com/file/d/1ewgLvmlfg3f0CVE3B6mu9wRtgK-TcORz/view?usp=sharing) 
* [DevSecOps: How Proactive Security Integration Reduces Your Agency's Risks & Vulnerability](https://gcn.com/whitepapers/2020/06/gitlab-devsecops-wp-060120.aspx?&pc=G0136GCN&utm_source=webmktg&utm_medium=E-Mail&utm_campaign=G0136GCN)
* [Hardened Containers: The Right Application Platform Can Help DoD Develop Its DevSecOps Culture](https://drive.google.com/file/d/1cszlj5fKFE12qFd4aMNij0a83v-3Fe4r/view?usp=sharing)
* [GitLab and the USDS Playbook: Delivering on the Promise of Digital Services (whitepaper)](https://page.gitlab.com/resources-ebook-gitlab-usds-playbook.html)
* [VPAT Template](https://docs.google.com/document/d/1o6gCe2Se_Cuia-VsrUTdGvESNZVefyoqFAu1NY8rJpQ/)



***Coming Soon:***

* Application Security for the DevOps Lifecycle: The critical value of shifting security left (S&L focus)
* GitLab Security for the DoD & IC (DoD & IC focus)
* How to Build-In Collaboration Across Your Enterprise
* Empower Your Workforce: Automate Your DevOps & Shift to Higher Value Work
* Ways to Make Your DevOps Procurements More Agile
* Hardened Containers for the DOD Software Factory (DoD focus)
* Moving to the Cloud with Less Pain (S&L focus)
  
  
  

### Resources 

* [Most Commonly Used Sales Resources](https://about.gitlab.com/handbook/marketing/product-marketing/sales-resources/)
* [Competitive Intelligence Resources](https://about.gitlab.com/handbook/marketing/product-marketing/competitive/compete/)
* [Global PubSec Marketing Account Profile Template (outside of US)](https://docs.google.com/document/d/1SUS0fbtVpvN4y44AT-BAiPAD32Bbq-hW-CSZco7uiUQ/edit) (use this to compile account-specific knowledge to share with Marketing) 
* [Product Marketing Team Talk Inventory](https://about.gitlab.com/handbook/marketing/product-marketing/pmmteam/#pmm-talk-inventory)
* [Global PubSec Speakers Bureau (outside of US)](https://drive.google.com/drive/folders/1u47kBHDh9cqOuA683k0MxzwxrZmqobzR?usp=sharing)


### Getting PMM Support    
* [Strategic Marketing Request](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=A-SM-Support-Request)
* [Speaker Request]

