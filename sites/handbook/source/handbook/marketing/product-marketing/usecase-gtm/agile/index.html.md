---
layout: markdown_page
title: "Agile"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Who to contact

| PMM | TMM |
| --- | --- |
| Cormac Foster (@cfoster3) | Tye Davis (@davistye) |
| Brian Glanz (@brianglanz) |  |

# The Market Viewpoint

## Agile Planning and Management

By empowering teams, embracing change, and focusing on delivering value, Agile methodologies have transformed software development. 
Agile teams create more relevant, valuable, customer-centric products more quickly than ever before, but these new methodologies bring 
new challenges. 

Since Agile is not a single methodology but a set of guiding principles embraced by many methodologies in many ways, there 
is no single “best” way to plan and manage an Agile enterprise, and older project planning tools are often insufficiently flexible to 
address the needs of a given implementation. This functionality gap leads to poor project estimation, planning, and progress tracking. 
Furthermore, Agile methodologies can create confusion between teams and throughout the enterprise, where even well-documented Agile 
processes may not map to traditional success metrics and waterfall financial planning.

Oragnizations, need a better way to manage projects, programs, and portfolios using Agile methodologies. Specifically, they need to be able to:

* **plan** and prioritize work based on strategic objectives.
* **initiate** work.
* **monitor** the progress of that work in progress.
* **collaborate** on work, across teams, throughout the product lifecycle.
* **control** and optimize the flow of work.
* **close** work items when completed.
* **measure** the value created by that work.
* **communicate** that value throughout the organization, regardless of other methodologies in place.



## Personas

### User Persona
With GitLab's unique capability to connect every phase of the Software Development Lifecycle in a single DevOps platform, many user personas find a solution in Integrated Agile Planning.

User personas and their key motivations for using GitLab for Agile planning include:

#### [Parker the Product Manager](/handbook/marketing/product-marketing/roles-personas/#parker-product-manager)
- PMs coordinate feature development and project success, along with general workload. Abilities to monitor progress through commits and the review app, and to validate those changes and provide feedback, are key to their success.
- Over time, changes generate valuable statistical insight for PMs to assess progress with product development and other projects.

#### [Delaney the Development Team Lead](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
- Like PMs, Team Leads need to understand their team's capacity to assign upcoming tasks and meet goals on time.

In consideration: personas from an Agile Planning perspective might include and be named as:
* Scrum Master
* Product owner
* Developer
* Tester/QA
* Project managers
* Product managers

### Buyer Personas
Purchasing for Integrated Agile Planning may not require executive involvement, with one possible exception being the influence of the [VP Application Development](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager).

#### [Erin the Application Development Executive (VP, etc.)](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc)
- Erin is a strategic leader focused on business challenges and the big picture.
- Erin's top goal is predictable business results.

## Industry Analyst Resources

List key analyst coverage of this use case


# Market Requirements

**Map business and portfolio plans to market opportunities**

| Requirement |  Description  |  Typical features  |  Value |
|----------|-------------|----------------|-------|
| 1. Customer value tracking  |  Stores or accesses metrics that quantify value received by the customer.   |  Customer satisfaction, adoption, lifetime customer value  |  Provides an objective standard by which the effectiveness of efforts can be judged.  |  
| 2. Integrations to external data sources |  Provides visibility into and updating of external systems that contain data to management and planning  |  DevOps, financials, performance monitoring systems  |  Enables tracking of value that extends beyond scope of planning tool, storage of data in other systems for further analysis  |  

**Plan and collaborate on value delivery**

| Requirement |  Description  |  Typical features  |  Value |
|----------|-------------|----------------|-------|
| 1. Requirements modeling, management, and analysis  |  Describe necessary product behavior against which output can be validated.  |  Epics, Features, Stories, Tasks, Health Status Reporting, and any necessary compliance to manage requirements for regulated industries |  Provides a common vision for the product's end state and an archive of scope changes throughout the development process, Ensures that value being delivered meets stated goals and necessary compliance.  |  
| 2. Flexible workflow support  |  Enables events and workflows from popular Agile frameworks and custom Agile implementations  |  Sprint planning, sprint retrospectives, customizable timeboxes, support for enterprise Agile frameworks such as SAFe and LeSS  |  Increases productivity by reducing context switching, provides a single source of truth for onboarding, auditing, and reference, removes operational resistance to Agile adoption by working with existing methodologies.  |  
| 3. Role-specific interfaces  |  Interfaces that surface relevant information in a context suited to different users.  |  Boards, roadmaps, dashboards  |  Allows workers to inspect and interact with workflows without switching context from their primary work tools or sifting through information that is not necessary within their business context.  |
| 4. Actionability  |  Tactical participation in workstreams from any part of the management system  |  Inline collaboration, drill-downs from dashboards into work items  |  Closes the loop between ideation and iteration, allowing decision-makers to execute immediately and unblock value flows.  |  

**Report on Outcomes**

| Requirement |  Description  |  Typical features  |  Value |
|----------|-------------|----------------|-------|
| 1. Quality metrics  |  Usability and value of outputs  |  Test coverage, escape defect analysis, security vulnerabilities, license compliance  |  Improve product value, identifies opportunities for training and process refinement, minimize adverse business and compliance impacts of changes.  |  
| 2. Operational metrics  |  DevOps efficiency and success  |  Deployment Frequency, Lead Time, Cycle Time, Change Failure Rate  |  Identify waste, optimize delivery of value |  
| 3. Organizational effectiveness metrics  |  Efficiency with which the organization collaborates to deliver customer value  |  Team adoption of Agile processes, time tracking, financial / cost tracking and estimation  |  Surfaces systemic process inefficiencies and opportunities for improvement.  |  


# The GitLab Solution

## How GitLab Meets the Market Requirements

| Market Requirements | How GitLab Delivers | GitLab Category | Demos |
| ------ | ------ | ------ | ------ |
| REQMT | HOW | CATS | DEMO |

## Top 3 Differentiators and Key Features

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
|  **Product Development Management**  | GitLab is the only product that provides collaboration capabilities to Product teams that works not only with source code but also IP, graphic assets, animations, binaries, and general project management issues. | Forrester's [Adopt Product Management to Connect Design and Development](https://www.forrester.com/report/Adopt+Product+Management+To+Connect+Design+And+Development/-/E-RES149995) states "Siloed Design And Dev Teams Deliver Subpar Software"  |
| second differentiation  |  def  |  ghi  |
| **Single Application**  |  def  |  ghi  |

## [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

### Discovery Questions
- list key discovery questions

## Competitive Comparison
TBD - will be a comparison grid leveraging the capabilities

### Industry Analyst Relations (IAR) Plan (UPDATE AS NEEDED FOR THIS USECASE)
- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=1124037301).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.

## Proof Points - customers

### Quotes and reviews
- List of customer quotes/reviews from public sites

### Case Studies
- List of case studies

### References to help you close
- Link to SFDC list of use case specific references

## Adoption Guide
### Playbook Steps

- To be added...

### Adoption Recommendation


This table shows the recommended use cases to adopt, links to product documentation, the respective subscription tier for the use case, and telemetry metrics.

| Feature / Use Case                                  |    F/C    |   Basic   |    S/P    | G/U  | Notes                      | Telemetry |
| --------------------------------------------------- | :-------: | :-------: | :-------: | :--: | :------------------------- | :-------: |

### Enablement and Training

### Professional Service Offers

## Partners
- Describe how key partners help enable this use case

## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this UseCase

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this UseCase

## Direction
- Describe a vision for compelling future improvements

## Resources
### Presentations
* LINK

### Whitepapers and infographics
* LINK

### Videos (including basic demo videos)
* LINK

### Integrations Demo Videos
* LINK

### Clickthrough & Live Demos
* Link
