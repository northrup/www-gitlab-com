---
layout: handbook-page-toc
title: "Sales Development"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

As a Sales Development Representative (SDR), you focus on outreach, prospecting, and lead qualification. To do so, you need to have an understanding of not only product and industry knowledge, but also sales soft skills, and internal tools and processes. This handbook page will act as a guide to those topics as well as general information about the SDR team.

## Reaching the Sales Development Team (internally)

#### Slack Channels (by region)

* **Main Channel** = [`#sdr_global`](https://gitlab.slack.com/messages/C2V1KLY0Z)      
* **Announcements** = [`#sdr-fyi`](https://app.slack.com/client/T02592416/C011P828JRL)       
* **Conversations** = [`#sdr-conversations`](https://gitlab.slack.com/messages/CD6NDT44F)       

**AMER**
* **All** = [`#sdr_amer`](https://gitlab.slack.com/messages/CM2GAVC78)      
* **AMER SMB + LATAM + Public Sector** = [`#sdr_amer_small_latam_pub`](https://app.slack.com/client/T02592416/C013W1DH98Q)
* **AMER Mid-Market** = [`#sdr_amer_mm`](https://app.slack.com/client/T02592416/C014PHFNE2U)
* **East** = [`#sdr_amer_east`](https://gitlab.slack.com/messages/CGTF184EB)      
* **West** = [`#sdr_amer_west`](https://gitlab.slack.com/messages/CLU1A6BA5)
* **Named** = [`#sdr_amer_named`](https://app.slack.com/client/T02592416/CUFRP6U6Q)
* **Acceleration** = [`#sdr_acceleration_team`](https://gitlab.slack.com/messages/CKTJDKNRX)

**EMEA**
* **All** = [`#sdr_emea`](https://gitlab.slack.com/messages/CCULKLB71)      
* **Commercial** = [`#sdr_emea_commercial`](https://gitlab.slack.com/messages/CM0BYV7CM)
* **Enterprise** = [`#sdr_emea_enterprise`](https://gitlab.slack.com/messages/CR04GHRSN)
* **Acceleration** = [`#acceleration_emea`](https://gitlab.slack.com/messages/GV864K8KD)

**APAC**
* **All** = [`#sdr_apac`](https://gitlab.slack.com/messages/CM0BPBEQM)

#### Issue Boards & Team Labels 

[SDR Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/707128): used to track relevant GitLab issues involving the SDR team. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following *scoped* labels are used. 

[SDR Event Tracker](https://gitlab.com/groups/gitlab-com/-/boards/1718115): used to track upcoming events globally.

- `SDR` - issues concerning the SDR team. This label will typically be removed and changed to one of the below labels once accepted by our team.
- `SDR::Priority` - projects that we would like brought into RevOps meeting for feedback/next steps from other teams
- `SDR::Planning` - Discussion about next steps is in progress for issues concerning the SDR team
- `SDR::In Progress` - SDR action item is presently being worked on
- `SDR::On Hold` - SDR project is put on hold after agreement from SDR leadership team 
- `SDR::Watching` - No direct SDR action item at this time, but SDR awareness is needed for potential support/questions
- `SDR::Enablement Series` - Label to track and monitor upcoming topics for the SDR enablement series. All of these issue roll up to this epic.
- `SDR::AMER Event Awareness` - Americas SDR awareness is needed for potential support/questions in regards to events
- `SDR::APAC Event Awareness` - APAC SDR awareness is needed for potential support/questions in regards to events
- `SDR::EMEA Event Awareness` - EMEA SDR awareness is needed for potential support/questions in regards to events

## Quick Reference Guide

| Resource |
| :----: | 
|  [SDR onboarding page](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/) | 
|  [Tanuki Tech](/handbook/marketing/revenue-marketing/sdr/tanuki-tech/) |
|  [Sales handbook page](/handbook/sales/) | 
|  [Sales resources page](/handbook/sales/#quick-reference-guide) | 
|  [Go to Market page](/handbook/business-ops/resources/) | 
|  [SDR Manager resources page](/handbook/marketing/revenue-marketing/sdr/sdr-manager-resources) | 
|  [SDR job family/levels](https://about.gitlab.com/job-families/marketing/sales-development-representative/) | 

## Onboarding
In your first month at GitLab we want to help ensure you have everything you need to be successful in your job. You will go through enablement videos, live sessions and activities covering a wide range of getting started topics. 
- [SDR onboarding goals and process](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/)

## Making Changes to the GitLab Handbook (for SDRs)

[Video Walkthrough of how to make changes to the GitLab Handbook for the SDR org](https://drive.google.com/file/d/1BjvMNgQgimYZOnaX0NpgaFvbYl47Jcwa/view?usp=sharing)

One of our Values is being [handbook first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/). In order to align the SDR organization more closely to this ideal, below are suggested steps. Please remember that the Handbook is a living document, and you are strongly encouraged to make improvements and add changes. This is ESPECIALLY true when it comes to net new solutions that should be shared so that the whole organization has access to that process. (aka The DevOps ideal of turning "Localized Discoveries" into "Global Knowledge".)

Steps:

1. Have a change you want to make to the handbook? Great!
2. Navigate to the source code of the page in the handbook (e.g. [Link to edit the SDR page in the Handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/handbook/marketing/revenue-marketing/sdr/index.html.md) )
3. Click either "Edit" or "Web IDE" to make your changes.
4. Have a brief but descriptive "Commit message" (e.g. "Add new section on 'How to Make Changes to the Handbook'") and commit your changes
5. Fill out the Merge Request details

## Onboarding
In your first month at GitLab we want to help ensure you have everything you need to be successful in your job. You will go through enablement videos, live sessions and activities covering a wide range of getting started topics. 
- [SDR onboarding goals and process](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/)

## SDR Enablement

#### Weekly SDR Enablement Series
The SDR team has a weekly enablement series for 30-minutes every Wednesday. This series covers a wide range of training topics such as workflow/processes, campaigns, alignment with other teams, tool training, technical training etc.
- If you would like to run an enablement session on a specific topic, please fill out [this issue](https://gitlab.com/gitlab-com/marketing/sdr/-/issues/new?issuable_template=sdr_enablement_series_request).
- [Upcoming sessions:](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1102) The due date = the date of the session. 
- Recordings of previous sessions can be found in this [playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqGVrIWsGFbL7A2RzesgfdP) on GitLab Unflitered YouTube channel.

#### SDR Technical Development
As part of your [onboarding](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/), you will begin an SDR Technical Development course with our Senior Sales Development Solutions Architect. The goal of this course is to enable you to be more comfortable having technical discussions - specifically when it comes to GitLab’s use cases. Each level of the course is tied to our [levels](https://about.gitlab.com/job-families/marketing/sales-development-representative/#levels) in the SDR role.

## SDR Resources

| SDR Resources    |  Description       | 
| :---- | :---- |  
|  [SDR job family](https://about.gitlab.com/job-families/marketing/sales-development-representative/) | *Information on the SDR role and requirements for level progression* |
|  [SDR onboarding page](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/) | *Overview of SDR onboarding that includes an overview, process, expectations, and manager responsibilities* |
|  [SDR Manager resources page](/handbook/marketing/revenue-marketing/sdr/sdr-manager-resources) | *SDR Manager resources & links* |
|  [SDR Territory Alignment](https://drive.google.com/drive/search?q=%22Territories%20%20Mapping%20File%22%20owner:tscott%40gitlab.com)| *This document is currently the single source of truth for SDR/Territory Alignment* |
|  [Enterprise SDR Outbound Process Framework](https://drive.google.com/drive/search?q=%20Outbound%20Process%20Framework%22%40gitlab.com) | *Outbound process framework for the Enterprise SDR team. Note: These vary by team and geo*|
|  [SDR Outreach Collections]| *In development, to be added in Q3FY21 (maintained by SDR Content Lead)*
|  [SDR Enablement Videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KrjbznnEEiCtxUfT8-OV6X8)| *Enablement videos and how-tos for SDRs* |

| Marketing Resources    |  Description       | 
| :---- | :---- |  
|  [Marketing Resource Links](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit?usp=sharing)| *GitLab whitepapers, ebooks, webcasts, analyst reports and more for Sales & SDR education*|
|  [Marketing Events + Gated Content Assets + Webcasts](https://drive.google.com/drive/search?q=%22Events%20Gated%20Content%20Assets%22%20owner:jgragnola%40gitlab.com) | *SDRs can use this sheet to refer better understand the marketing assets that are being consumed by prospects. To view the ungated content, click on the link in the Pathfactory or PDF/YouTube columns. Note: Sharing non-gated assets requires SDR manager approval* |
|  [GitLab Buyer Personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/buyer-persona/)| *Resource to help GitLab sellers better understand our target audience*

| Sales Resources    |  Description       | 
| :---- | :---- |  
|  [Sales handbook page](/handbook/sales/) | *GitLab Sales team handbook* |
|  [Sales resources page](/handbook/sales/#quick-reference-guide) | *Link to the Sales quick reference guide with links to learn more about the various sales teams & intiatieves* |
|  [Weekly sales enablement](https://about.gitlab.com/handbook/sales/training/sales-enablement-sessions/) | *These sessions take place every Thursday and SDRs have an open invitation to participate* |
|  [Go to Market page](/handbook/business-ops/resources/) | *Link to GitLab's go-to-market strategy including: definitions, processes, and record management* |
|  [Sales Training handbook page](/handbook/sales/training/) | *Link to GitLab sales training* | 
|  [Command of the Message](/handbook/sales/command-of-the-message/) | *"Command of the Message" training and the GitLab valueframework* |
|  [Most commonly used sales resources](/handbook/marketing/product-marketing/sales-resources/)| *Sales resources page*
|  [Flash Field newsletter](/handbook/sales/field-communications/field-flash-newsletter/)| *Learn more about sales' weekly newsletter*

## Segmentation
The SDR team aligns to the [Commercial](handbook/sales/commercial/) and Large sales teams. These teams are broken down into three segments: Large, Mid-Market and SMB which are based on the total employee count of the Global account. *Note: The commercial sales team includes both Mid-Market and SMB. This segmentation allows SDRs and Sales to be targeted in their approach and messaging. The segments are aligned to a region/vertical and then divided one step further via territories in the regions. Our single source of truth for determining number of employees is DataFox followed by DiscoverOrg. 
* [Sales segmentation](/handbook/business-ops/resources/#segmentation)
* [Sales territories](/handbook/sales/territories/)
* [Determining if a lead is in your territory](/handbook/business-ops/resources/#account-ownership-rules-of-engagement)
* [SDR and Sales alignment](/handbook/marketing/revenue-marketing/sdr/sales-sdr-alignment/) - working with your SAL or AE!

**SDR Team Breakdown**
Geo = SDR teams that align directly to sales territories and AE/SALs that work a geo territory
     *  Commercial (MM & SMB)
     *  West Enterprise
     *  East Enterprise
[Named](/handbook/marketing/revenue-marketing/sdr/#named-sdr-team) = SDR team that supports only Named SALs/Accounts
[Acceleration](/handbook/marketing/revenue-marketing/sdr/#acceleration-marketing) = Aligns to account based marketing target accounts and Large geo territories for a period of 60 days in which they will work closely with the geo SDR to warm up the territory. To read more about this alignment please head over to the [Account Based Marketing page](/handbook/marketing/revenue-marketing/account-based-marketing/)

## SDR Compensation and Quota

SDR quota is made up of the following depending on [sales segment](/handbook/marketing/revenue-marketing/sdr/#segmentation):
- [Sales Accepted Opportunities (SAOs)](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)
- [Initial Qualified Meetings (IQM)](/handbook/marketing/revenue-marketing/sdr/#qualified-meeting)
- [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) pipeline component 

**Activity & Results Metrics**

While the below measurements do not impact your quota attainment, they are monitored by SDR leadership. 
* Results
  * Pipeline value of SDR sourced opportunities
  * IACV won from opportunities SDR sources
* Activity
  * % of named accounts contacted
  * Number of opportunities created
  * Number of calls made
  * Number of personalized emails sent
  * Daily outbound metrics 
    * At least 30 emails/day
    * At least 30 dials/day

## SDR Standards
* Meet monthly quota of Sales Accepted Opportunities (SAOs) and IQM and iACV pipeline targets you have been set.
* Be able to identify where a prospective customer is in their buying cycle and take appropriate action to help them along their journey towards becoming a customer.
* Have a sense of urgency - faster response time directly influences conversion rates.
* Work lead records within Salesforce by leveraging sequences in Outreach.
* Maintain a sense of ownership of data integrity in Salesforce and Outreach: cleaning up and assuring accuracy and consistency of data.
* Adding any information gathered about a LEAD, CONTACT, or ACCOUNT from the data sources Datafox & DiscoverOrg where you can.
* Attend each initial qualifying meeting (IQM) with your AE/SAL. Documenting notes in SFDC and communicating with your AE/SAL before and after the meeting.

## SDR Tools
* [Salesforce](/handbook/business-ops/tech-stack/#salesforce)
* [Outreach.io](/handbook/marketing/marketing-operations/outreach/)
* [DataFox](/handbook/business-ops/tech-stack/#datafox)
* [DiscoverOrg](/handbook/business-ops/tech-stack/#discoverorg)
* [LinkedIn Sales Navigator](/handbook/business-ops/tech-stack/#linkedin-sales-navigator)
* [Chorus](/handbook/business-ops/tech-stack/#chorus)
* [Drift](/handbook/marketing/marketing-operations/drift/)
* [LeanData](/handbook/marketing/marketing-operations/leandata/)

## SDR Workflow & Process
As an SDR, you will be focused on leads - both inbound and outbound. At the highest level, a lead is a person who shows interest in GitLab through inbound lead generation tactics or through outbound prospecting.
- [Updating](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/sdr-manager-resources/#lead-routing--alignment-resources) territory alignment, `SDR Assigned` field, lead routing, Drift alignment/team 

### Qualified Meeting

**What is considered a Qualified Meeting?**

A Qualified Meeting is a meeting that occurs with a prospect who is directly involved in a project or team related to the potential purchase of GitLab within a buying group, either as an evaluator, decision maker, technical buyer, or *influencer* within an SDR Target Account. 
To be considered qualified, the meeting must occur (prospect attends) and new information must be uncovered that better positions the SAL and Regional SDR to create a SAO with that particular account based on the SAO Criteria as stated in the handbook.

**Qualified Meeting Process**

A “New Event” IQM gives an SDR the ability to track every meeting that is scheduled and held in Salesforce without having to prematurely create a net new opportunity. SDRs can tie any event (IQM) to an existing account, opportunity, etc. The purpose of creating an IQM in salesforce is to provide the ability to track how many IQMs are being created per SDR and what the results are of these IQMs. 

The purpose of creating an Initial Qualifying Meeting (IQM) in Salesforce is to track top of funnel pipeline meetings that are booked, occur, and convert to Sales Accepted Opportunities (SAOs). SDRs can track every meeting that is scheduled and held in Salesforce without having to prematurely create a net new opportunity prematurely. SDRs can tie any IQM to an existing account or opportunity from a Lead or Contact record. 

**How to create an IQM in SFDC**

Within Salesforce there is a button titled “New Event” which you can access in the global actions bar on your Salesforce “Home Screen”, and in the “Open Activity” section of any Lead, Contact, Account, and Opportunity record. [This image](https://docs.google.com/document/d/1NUP7Ze_e-1sKGAELim-_yaLDnN9rf0wVKWfug0lhtyI/edit?usp=sharing) will show you where to click!

If creating an Event: IQM under a Lead or Contact record, click the event button in the global actions bar and complete all relevant fields which should include your pre-IQM notes in the description field. 

Once a meeting with a prospect is confirmed, the SDR will send either a Google calendar invite to the prospect (include their SAL) and/or send a meeting in Outreach.io with a proposed agenda. SDR should share Event IQM SFDC link with SAL prior to IQM occuring - scheduling a pre/post IQM sync is best practice. 

Once the IQM occurs, then the SDR can add their Post-IQM notes to the Description Field and update the Event Disposition accordingly. If the IQM meets criteria and has next steps, then it's an SAO and the SDR should convert Lead to Contact record and create new opportunity with same process as outlined in Handbook. If the IQM occured and the SDR/SAL/AE uncovered helpful information but ultimately, there are no next steps, the the SDR should dispostiion Event IQM as Qualified Meeting (do not create a new opportunity). If Event IQM was a no-show and/or needs to be rescheduled, then SDR owns reaching out to prospect to reschedule and dispoistions Event IQM accordingly. 

**How to Create an IQM in Outreach**

In Outreach, you have the ability to book a meeting directly from a prospect overview. Even better, this can be done while you’re on a call with a prospect or immediately after without having to leave Outreach. Booking a meeting from Outreach will automatically pull the following fields directly into an SFDC event object:
* Subject (Lead/Contact Name - IQM)
* Location (Zoom Link)
* Start & End times
* Assigned To (SDR name)
* Event Disposition (Required)
* Activity Source (Required)
* Booked By
* Attributed Sequence Name
* Description (add Pre/Post IQM Notes)
* Related To (You can select a parent opportunity if applicable or relate to the opportunity you create from Qualified Meetings)

Select “book meeting” from the prospect actions menu. For a visual/video walkthrough of this process take a look [here](https://docs.google.com/document/d/1oB3RsjqCIUoXvX3F4DTrYOLh4X0K-7tzUPs3jYAQrwg/edit?usp=sharinghttps://docs.google.com/document/d/1oB3RsjqCIUoXvX3F4DTrYOLh4X0K-7tzUPs3jYAQrwg/edit?usp=sharing).

### Inbound Lead Generation

Inbound lead generation often uses digital channels - social media, email, mobile/web apps, search engines, websites, etc - as well as in-person marketing activities to meet potential buyers where they are. When people interact with GitLab, we use lead scoring to assign a numerical value, or points, to each of these leads based on their actions and the information we have about them. Once a lead reaches 90 points, they are considered a [Marketo Qualified Lead](/handbook/business-ops/resources/#mql-definition) or MQL.

### Working Inbound Leads

SDRs are responsible for following up with MQLs by reviewing their information, reaching out, and working with them to understand their goals, needs, and problems. Once you have that information, you can use our [qualification criteria](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao) to determine if this is someone who has strong potential to purchase our product and therefore should be connected with sales for next steps. As you are the connection between Marketing and Sales you want to make sure every lead you pass to the Sales team is as qualified as possible.

These MQLs will show up in your lead views in Salesforce. The views, listed below, allow you to see your leads in a categorized way to simplify your workflow. Leads are routed to you and flow into your views via the tool [LeanData](/handbook/marketing/marketing-operations/leandata/) which takes each lead through a series of conditional questions to ensure it goes to the right person. Even though all SDRs leverage the same views, they will only show you leads that have been specifically routed to you. You will be responsible for following up with all of the leads in your MQL views by sequencing them using [Outreach.io](/handbook/marketing/marketing-operations/outreach/). Once sequenced, their lead status will change and they will move from your MQL views allowing you to have an empty view. Managers monitor all views to ensure they are cleared out. 

#### Lead Views
* My MQLs - all leads that have accumulated [90 points based on demographic/firmographic and/or behavioral information](/handbook/marketing/marketing-operations/#lead-scoring-lead-lifecycle-and-mql-criteria). These leads should always be your first priority. 
     * Important to note: If one of your leads submits a contact us form, you will receive an email alert notifying you to take action quickly in addition to the lead showing up in your My MQL view.
* My Inquiries - leads that haven't quite reached 90 points yet. These leads are to be looked at once your MQLs are worked. 
* My Qualifying - leads that you are actively qualifying in a back and forth conversation
* My Leads w/new LIM - Stale MQL, Nurture, Raw w/new LIM - leads that at one point were an MQL or had a status of nurture or raw but have recently taken an action. 

#### Contact Views

You  also have two contact views. COMING SOON: routing contacts to these contact views if someone is already a contact in our system but has taken a lead qualifying action or requested contact. After a certain amount of time, the record ownership will transition back to SAL/AE ownership.
*  My MQLs - all contacts that have taken a recent lead qualifying action. Determine if this contact is closely engaged with Sales and reach out accordingly
*  My Qualifying - contacts you are actively qualifying in a back and forth conversation

#### Lead and Contact Status / Outreach Automation

Lead/contact statuses allow anyone in Salesforce to understand where a lead is at in their journey. The automation mentioned below is all done through an Outreach.io trigger. 

* Once a lead has been placed in an Outreach sequence, the lead status will automatically change from MQL, Inquiry or Raw to Accepted marking that you are actively working this lead. 
* When a lead responds to you via email, their status will again automatically change. This time it will change from Accepted to Qualifying. *If you are not working on qualifying this lead further, you will need to manually change the status to Nurture so that this lead is back in Marketing nurture and isn’t stuck in your qualifying view. 
* If a lead finishes an Outreach sequence without responding, the lead status will automatically change to unresponsive or nurture in three days if there is still no response.
* The only time you will need to manually change lead status outside of what is mention in the previous bullets is if you for some reason don't use an Outreach sequence to reach out to someone or if you need to unqualify a lead for bad data etc.
* If you check the `Inactive lead` or `Inactive contact` checkbox, signifying that this person no longer works at the company, any running sequence will automatically be marked as finished. 

### Inbound Lead Workflow Checklist

**IMPORTANT:** For Mid Market SDR reps, if the account your lead matches has the account type 'customer,' this lead will need to be worked by your aligned AE. Update the lead owner and let them know. You can determine this via the LeanData section in Salesforce or by leveraging the 'Find Duplicates' button at the top of the lead in Salesforce. Look to see if the account this lead belongs to has a CARR value (total annual recurring revenue this customer brings) associated with it. Lastly, you can [search for the account](/handbook/support/workflows/looking_up_customer_account_details.html#finding-the-customers-organization) to look for the CARR value. Convert the lead to a contact and attach it to the customer account on conversion. 

**Checklist**
* Make sure the lead is in your territory/segmentation
    * [Parent/Child Segmentation:](/handbook/business-ops/resources/#account-ownership-rules-of-engagement) all accounts in a hierarchy will adopt the MAX segmentation of any account in the hierarchy.
    * If the lead is not yours, please route it to the appropriate rep, after updating it with the information that helped you determine it was not yours. Assigning to the `SDR Assigned` user is acceptable.
* Make sure name is capitalized correctly
* Check for duplicates and merge or route to the appropriate rep by using the `Find Duplicates` button at the top of the lead page. The Find Duplicate page lists Accounts where you might find the `SDR Assigned`
    * [How to merge leads](https://drive.google.com/file/d/1I4MsYvVdAcxA__jHcKQddlFIpzWnzhA3/view?usp=sharing)
* Add or update any missing information on the lead. The SDR team is responsible for making sure lead data is up to date and as detailed as possible.
    * Check Datafox to confirm and/or add company location, phone number and employees.
        * If there is no Datafox information, use DiscoverOrg and lastly LinkedIn
        * If you feel there is a discrepancy in segementation/total employee count, you can adress that by following [this](/handbook/business-ops/resources/#sales-segment-and-hierarchy-review-process) process.
    * If you have a company email domain, use the LinkedIn button at the top to help find the lead's job title  
* Has this person opted out of GitLab communication? In order to remain compliant we cannot follow up via phone or email as we don’t have granular opt-out. You need to change lead status to unqualified and unqualified reason to unsubscribe.

**Outreach & Marketo**

If the lead is yours to work based on all of the above, sequence them in Outreach using the master sequence that correlates with the Last Interesting Moment. You can also look at the initial source and Marketo insights to get a holistic view of what a lead is looking into. There is an Outreach collection of sequences for inbound leads for each region. These collections contain a master sequence for nearly every inbound lead scenario. 
* **Master Sequence**: a sequence created by SDR leadership that should be used by all reps to follow up with inbound leads
*  [**Sequence collection**](https://support.outreach.io/hc/en-us/articles/360009145833-Collections): group of sequences compiled by region
*  **Last Interesting Moment**: data pulled in from our marketing auotomation software, [Marketo](/handbook/marketing/marketing-operations/marketo/), that tells you the last action a lead took.
*  [**Initial source**](https://about.gitlab.com/handbook/business-ops/resources/#initial-source): first known action someone took when they entered our database
*  **Marketo Sales Insights (MSI)**: a section on the lead/contact in Salesforce that shows you compiled data around actions a lead/contact has taken
*  **High touch and low touch sequences**: a high touch sequence should be used for high-quality leads. High touch sequences require you to add in more personalization and have more touch points across a longer period of time. Low touch sequneces are typically automated and run for a shorter period of time. A high quality lead has valid information for at least two of the following fields:
    * Company email domain or company name
    * Phone number
    * Title

### Qualification Criteria and SAOs

Qualification criteria is a minimum set of characteristics that a lead must have in order to be passed to sales and become a Sales Accepted Opportunity (SAO). You will work to connect with leads that you get a response from to obtain this information while assisting them with questions or walking them through how GitLab might be able to help with their current needs. The qualification criteria listed in the linked handbook page below aligns to the 'Qualification Questions' sections on the LEAD, CONTACT, and OPPORTUNITY object in Salesforce. In order to obtain an SAO you will need to have the 'required' information filled out on the opportunity.  
*  [Qualification criteria needed](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)

When a lead meets the qualification criteria and you have an IQM/intro call scheduled with your AE/SAL, you will convert the lead to an opportunity. 
* [ How to create an opportunity](/handbook/business-ops/resources/#creating-a-new-business-opportunity-from-lead-record)

### Questions about a lead?

*  To determine where a lead should go if it was routed to you mistakenly, use the [Territories Mapping File - SSoT](https://docs.google.com/spreadsheets/u/0/?tgif=d&q=territories%20mapping%20file%20ssot)
*  If you have a question about the behavior of a lead or any other operational issue, use the slack channel #mktgops

**Technical questions from leads?**
* [Docs](https://docs.gitlab.com/)
* Slack channels
    * #questions
    * #support_self-managed
    * #support_gitlab-com

### Working with 3rd Parties

1.  Gather billing and end user details from the reseller:
    * Billing company name/address:
    * Billing company contact/email address:
    * End user company name/address:
    * End user contact/email address:
    * [Snippet in outreach](https://app1a.outreach.io/snippets/362)
2. Create a new lead record with end user details 
    *  Ensure that all notes are copied over to new LEAD as this is the LEAD that will be converted.
3. Converting the new lead
    * Name opp to reflect reseller involvement as shown here: “End user account name via reseller account name”
4. Convert original reseller lead to a contact associated with the reseller account
    *  If an account does not already exist for the reseller, create one when converting the lead to a contact.
    *  Assign record to the same account owner
    *  Do NOT create a new opportunity with this lead.
5. Attach activity to the opportunity
    * On the reseller contact, go to the activity and link each activity related to your opportunity to the opp.
        * Activity History > click edit to the left of the activity > choose 'opportunity' from the 'related to' dropdown > find the new opportunity > save
6. Update the opportunity
    * Change the business type to new business and stage to pending acceptance.
    * Under contacts, add the reseller contact, role as reseller, and primary contact.
    * Under partners, add the reseller account as VAR/Reseller"

### Drift Chat Platform
We use a the chat platform Drift to engage site visitors. As an SDR, you are expected to be active on Drift throughout your work day. The [tool's handbook page](/handbook/marketing/marketing-operations/drift/) will walk you through guidlines, best practices and troubleshooting. 

## Outbound Prospecting 

Outbound lead generation is done through prospecting to people who fall into our target audience and could be a great fit for our product. Prospecting is the process of finding and developing new business through searching for potential customers with the end goal of moving these people through the sales funnel until they eventually convert into customers. 

You will work closely with your dedicated SAL or AE to build a strategy for certain companies; or parts of your territory or personas you as an aligned team want to target. It is crucial that you are very intentional and strategic in your approach and always keep the customer experience in mind. When reaching out, offer value and become a trusted advisor to ensure we always leave a positive impression whether there is current demand or not.

You can target Closed Lost Opportunties once they are older than 30 days, in line with the Sales Handbook [Opportunity Stages](https://about.gitlab.com/handbook/business-ops/resources/#opportunity-stages)  
You can target Leads 60 days after the date of their last handraising activity [Active vs. Passive](https://about.gitlab.com/handbook/business-ops/resources/#active-vs-passive)



### Targeted Accounts

**Targeted account**: an account in which you as an SDR have developed an account plan with your AE or SAL and are actively targeting via outbound prospecting.

Targeted Accounts should be kept up-to-date in Salesforce in real-time, and checked as soon as you begin the account planning process. If the account does not have activity associated with leads/contacts over a 30-day period, the account should not be flagged as targeted. Each SDR is allowed 40 targeted accounts at any given time. If you feel this is too low, please connect with your manager.

If you check the targeted checkbox, you are committed to:
* Developing an account plan with your AE or SAL
* Actively outbounding to ideal personas leveraging person

Keeping your targeted accounts up to date is especially crucial when the Acceleration Team is entering your territory. The Acceleration Team typically generates a regional account target list 60-days prior to entering a region. At that point, any SDR target accounts” will be suppressed. Two weeks prior to kickoff, the Acceleration Team will schedule a kickoff call to:
* Review coverage dates
* Review target account list
* Ensure that targeted accounts and regional suppression lists are up-to-date
* Discuss account plans / targeting strategy

### Outbound Workflow
**IMPORTANT**: EMEA reps, old leads/contacts can only be called or emailed if they were in our system from May 2018 or later. Any lead/contact that was brought into our system prior would need to opt in to communication. Any new leads/contacts can only be called or emailed if they have been brought in from DiscoverOrg. You can also leverage LinkedIn Sales Navigator. 

The [SDR outbound framework](https://drive.google.com/drive/search?q=%20Outbound%20Process%20Framework%22%40gitlab.com) will walk you through both strategy and execution of outbound prospecting.

### Event Promotion and Follow Up Assistance

The SDR team will assist the Field Marketing and Corporate Marketing teams with campaign and event promotion and follow up. Learn more about [Field](/handbook/marketing/revenue-marketing/field-marketing/) or [Corporate Marketing](/handbook/marketing/corporate-marketing/) via their linked handbook pages. When these teams begin preparing for an event, they will create an issue using an issue template. Within that template is a section titled ‘Outreach and Follow Up’ for the SDR team to understand what is needed from them for each event. 
*  [Field Event Template](https://gitlab.com/gitlab-com/marketing/field-marketing/tree/master/.gitlab/issue_templates)
*  [Corporate Event SDR Support Template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/Event-SDR-Support-Template.md)

#### Field Process

When our Field Marketing Managers (FMMs) begin preparing for an event or micro campaign, they will create an issue using a [regional issue template](https://gitlab.com/gitlab-com/marketing/field-marketing/tree/master/.gitlab/issue_templates). Within that template is a section titled ‘Outreach and Follow Up’ for the SDR team to understand what is needed from them.

The FMM will loop in the appropriate SDR manager(s) on this issue as well as fill out/ensure the following sections are accurate:
* Pre-event/campaign goals
* Post-event/campaign goals
* Targets (title and number of attendees) 
* Post-event needs

Once the sections are populated and up to date, the SDR manager is looped in. They will then elect an SDR with bandwidth to begin completing the steps in each of the above sections. The SDR will: 
* Read the issue to get an understanding of the event
* Complete the tasks under the ‘Outreach and Follow Up’ section based on the deadlines next to each bullet. 
* Slack their manager and/or the FMM if they have questions. 

#### Deadlines
**Event Promotion**

*  The SDR manager needs to be assigned a minimum of 30-days prior to the event, but the sooner they can start prepping, the better.
*  The SDR manager(s) should have their teams begin compiling lists as soon as possible. Target lists need to be completed three weeks prior to the event so that we will have three weeks of promotion
*  The SDR project lead needs to have their target list as well as the shared sequence completed, reviewed and approved at least three weeks prior to event so that we will have three weeks of promotion

**Event Follow Up**

*  SDR project lead needs to have the follow up sequence created and reviewed with management and FMM one week prior to event. 
*  FMM will ensure leads from Field Marketing initiatives will be routed to the SDR within 48 hours.

**Sequence Information**
- Please clone one of the Master Event Sequences found in the [Events Outreach collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=6) and pre populate as many of the variables as possible. If all of the variables can be populated manually or through Outreach/Salesforce, change the email type from ‘manual’ to ‘auto.’ Work with your manager and the FMM to make any other additional adjustments that may be needed.

#### Corporate Process
The corporate process is the same as the field process detailed above with the exception that they have a seperate issue when looping in the SDR team for assitance that will link to the larger event issue. 

### Working with the Community Advocacy Team
[Community Advocates](/handbook/marketing/community-relations/community-advocacy/) support the [Education](/handbook/marketing/community-relations/education-program), [Open Source](/handbook/marketing/community-relations/opensource-program) and [Startups](/solutions/startups) Programs, and their Program Managers. When a lead fills out the form to apply for one of these programs, Salesforce lead ownership will automatically change to the Community Advocate Queue. If this lead was in an Outreach sequence, it will automatically be marked as finished. The Community Advocacy team will then work to qualify the lead. If the lead does not end up qualifying for one of the programs, they will be passed straight to sales.
- Forms
     -   [GitLab for education](https://about.gitlab.com/solutions/education/)
     -   [Startups](https://about.gitlab.com/solutions/startups/) 
     -   [Open Source](https://about.gitlab.com/solutions/open-source/program/)
- Questions
     - Chatter them at @CommunityAdvocates in Salesforce
     - Slack channel: #education-oss

## SDR Team Overview

| Team | Account Set | Marketing Support & Strategy | SDR Outreach & Strategy | Outbound Leads | Inbound Leads |
| ------ | ------ | ------ | ------ | ------ | ------ |
| **ABM SDR** | ~10 to ~100 accounts globally depending on tier. Highest value target accounts. Perfect to Strong fit with criteria. | Each account receives a customized marketing plan w/ personalized content. Each account treated as a market of one. Exact strategy depends on tier. | Outreach is highly targeted and customized to the account. SDR will be prepped on Marketing support their accounts will receive. | Gather specific prospects in DiscoverOrg and LinkedIn Sales Navigator based on GitLab's role and persona information. | Receives inbound leads based on account alignment as listed in account set column. |
| **Named SDR** | ~30 accounts / SAL. Specific accounts owned and worked by the designated SAL and paired SDR regardless of corporate headquarters. | Marketing tactics and events drive MQLs to SDRs. SDRs align with marketing to craft specific messaging based on marketing tactic. | Targeted net new account-centric and persona-based outreach based on sales alignment to educate and create internal champions that align with the GitLab value drivers and use cases. | Gather specific prospects in DiscoverOrg and LinkedIn Sales Navigator based on GitLab's role and persona information. | Receives inbound leads based on account alignment as listed in account set column. |
| **Geo SDR** | All remaining accounts. Alignment is a mix of territory-based, account-based, and round-robin depending on team and region. | Marketing tactics and events drive MQLs to SDRs. SDRs align with marketing to craft specific messaging based on marketing tactic. | Targeted net new account-centric and persona-based outreach based on sales alignment to educate and create internal champions that align with the GitLab value drivers and use cases. | Gather specific prospects in DiscoverOrg and LinkedIn Sales Navigator based on GitLab's role and persona information. | Receives inbound leads based on account alignment as listed in account set column. |

# PubSec AMER SDRs
We currently have 2 SDR's focused on supporting the PubSec ISR's. Their alignment is as follows:

| PubSec SDR | Group | 
| ------ | ------ |
| Evan Mathis| National Security Group (NSG 3-6) |
| Evan Mathis | Civilian | 
| Evan Mathis | State, Local, Edu (SLED) | 
| Josh Downey| Department of Defense (DoD Agencies, Army, Navy, Airforce) |
| Josh Downey | National Security Group (NSG 1,2)  | 
| Josh Downey | Systems Integrators (SI) | 

PubSec SDRs have a different workflow from other SDRs in that they do not convert leads into opportunities. They input qualification notes on the lead record after their intro call and then set up the IQM with the ISR. The ISR converts the lead into the opportunity to ensure the opp is assigned to the correct account. Once the ISR has determined our SAO criteria has been met, they are responsible for putting the SDR on the opportunity and moving the opportunity into discovery. 

The PubSec SDRs will work closely with PubSec FMM's and MPM's on pre and post event outreach. For events that are limited in attendance capacity, the PubSec ISRs will own pre and post event strategy, not the SDR's. 

[SDR PubSec Issue Board](//gitlab.com/groups/gitlab-com/-/boards/1864446?label_name[]=SDR%20Pub%20Sec) - used to track relevant GitLab issues involving the PubSec SDR team. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following scoped labels are used.


- `SDR Pub Sec`- Issues concerning the PubSec SDR team. When PubSec FMM's/MPM's add this tag to an event, these issues will appear on our board. When applicable, the PubSec SDR manager will create a SDR follow up issue and relate it to the event issue so that marketing members can easily track our work. 
- `SDR Pub Sec:: FYI` - Needs attention.
- `SDR Pub Sec:: In Progress` - Related work needed is in progress.
- `SDR Pub Sec:: Completed` - Related work is completed and ready for SDR manager to review/close.
- `SDR Pub Sec:: Revisiting` - Campaign is being revisited and additional outreach is being done.
- `SDR Pub Sec:: Evan FYI` - Issues pertaining specifically to Evan.
- `SDR Pub Sec:: Josh FYI` - Issues pertaining specifically to Josh. 


# Named SDR Team 
The Named SDR team (Amer-based) is responsible for creating qualified meetings and sales accepted opportunities for Strategic Named SALs. Their focus is on Large ‘Named’ accounts with a land and expand strategy. The team is composed of SDRs who focus on account-centric, persona-based outreach, strategic outbound prospecting, social selling and creative outreach to educate and create internal champions that align with the GitLab value drivers and use cases. 

# ABM Acceleration Team

The ABM Acceleration team is responsible for generating awareness, qualified meetings, and pipeline for the GitLab sales team. The team is made up of SDRs and specialists who use tactics like strategic segmentation, industry- and persona-based outreach, outbound prospecting, social selling, warm calling, and creative outreach to evangelize and educate prospects on what is possible with GitLab. There are five primary focus areas for the Acceleration team in FY21:

1.  [ABM Account Management - AMER](/handbook/marketing/revenue-marketing/account-based-marketing/#sdrabm-alignment) 
2.  [Native Geo Targeting - EMEA](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#emea-abm-acceleration)
3.  [Territory Warm Up - Global](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#helpful-definitions)
4.  [Strategic Content Development - Global](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#strategic-content-lead)

## Alignment, Roles, Responsibilities

The ABM Acceleration team aligns to the Tier 1 and Tier 2 account based marketing accounts (ABM) and is also responsible for "Territory Warm-up" where they align to Large geo territories for a period of 60-90 days. To read more about this alignment check out the [Account Based Marketing page](/handbook/marketing/revenue-marketing/account-based-marketing/).

| Region         |  **Role**             | Acceleration SDR      | 
| :------------- |  :--------------------------------- | :----------------- | 
| AMER         |  ABM / Accel SDR   | Shawn Winters     | 
| AMER           |  ABM / Accel SDR         | David Fisher   |
| EMEA - France & Eastern Europe          |  ABM / Accel SDR    | Anthony Seguillon     | 
| EMEA - Germany & CH/AUS           |  ABM / Accel SDR           | Christina Souleles    |
| Global           |  Content Lead         | Michael LeBeau  |
| Global           |  Segmentation Lead       | Kevin McKinley  |

### AMER ABM Acceleration

In AMER, the ABM Acceleration team is aligned to designated Tier 1 and 2 ABM accounts and responsible for:
* Leading account planning efforts and creating in-depth account plans for each target account
* Working with internal teams on campaigns and initiatives to support ABM
* Understanding the campaign briefs and becoming vertical experts
* Converting inbound leads and Drift conversations 
* Aligning outbound prospecting efforts to the GitLab and ABM campaigns
* Educating prospects and making positive impressions
* Generating awareness, meetings, and pipeline for sales

### EMEA ABM Acceleration

EMEA ABM Acceleration SDRs are aligned to **Native Geos** where they are responsible for:
* Working with SALs to identify a top Target Account list (ie a Top 20 or Top 50)
* Working with regional SDRs to determine the outbound strategy for the region
* Aligning targeted prospecting efforts to the GitLab and ABM campaigns
* Working with internal teams on campaigns and initiatives to support the regions
* Educating prospects and making positive impressions
* Attending in-region events
* Generating awareness, meetings, and pipeline for sales

EMEA ABM Acceleration SDRs are also responsible for executing **Territory Warm-ups** on an as needed basis. Responsibilities include:
* Campaign planning
* Working with the regional SAL & SDR teams to review campaign strategy, identify top target accounts, identify suppression accounts, and gather actionable intelligence
* Outbound prospecting to both top target accounts and net new accounts 
* Educating prospects and making positive impressions
* Generating awareness, meetings, and pipeline for sales

### Strategic Content Lead
The strategic content lead is responsible for creating, managing, and continuously improving SDR content. This indivudal will work closely with internal marketing and sales teams to create messaging, collateral, and assets for SDRs to use in their outreach.

- **Q2 FY21:** 
     - Review & catalog top performing SDR messaging 
     - Create a snippet library for SDRs to use in messaging
     - Identify gaps with existing content & manage the creation of new content, messaging, and assets 
     - Create working relationships and feedback loops with other marketing teams (PMM, MPM, SDR Leadership)

- **Q3 FY21:**
     - Buyer Persona content development
     - Field / Campaigns content support
     - Further develop outbound SDR snippet library

## Expectations

*  Meet monthly quota
*  Maintain a high sense of autonomy to focus on what's most important
*  Participate and lead planning, execution, and cross-functional meetings 
*  Participate in initial qualifying meetings, discovery calls, and follow-up conversations
*  Maintain exceptional Salesforce hygiene, logging all prospecting activity, conversations, and account intelligence uncovered
*  Incorporate and train Regional SDRs & SALs on the ABM Acceleration processes
*  Generate IACV Pipeline

## Issue Boards & Labels

The Acceleration team works from the issue boards listed here: [ABM Acceleration](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1333112?scope=all&utf8=%E2%9C%93&state=opened), [SDR Content](https://gitlab.com/gitlab-com/marketing/sdr/-/boards/1770804), and SDR Segmentation. If you are needing our assistance with any project, please open an issue on the [SDR issue board](https://gitlab.com/gitlab-com/marketing/sdr/-/issues) and use one of the labels or templates listed below.  

Global labels to use for: **Acceleration ABM**
- Need assistance from the ABM Acceleration team? Submit an issue and use the label `accel abm::queue`

Global labels for: **Strategic Content**
- Do you need help with messaging or a new sequence created? Create an SDR issue using the `SDR Content Request` issue template
- Do you have an idea or a project for the SDR content lead? Create an issue and use the label `SDR Content::FYI`
    - `SDR - High`: High Priority - SDR Content Request
    - `SDR - Medium`: Medium Priority - SDR Content Request
    - `SDR - Low`: Low Priority - SDR Content Request

## Outbound Strategies & Tactics

### Account Based SDR Coverage

Beginning Q2 FY21, we will be pairing the acceleration team and account-based team in order to align our most senior SDR’s to our highest value accounts while also solving for the need to accelerate sales onboarding and warming up sales territories. In parallel, we will also be incorporating the mapped SDR team in region into the plan and motions of the acceleration teams so as to encourage the transition to more acceleration based tactics across the entire organization, utilizing the intent data and other tools through the account based team.

- [ABM SDR Alignment](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-marketing/#sdrabm-alignment)
- [ABM SDR Account Handoff](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-marketing/#sdr-abm-account-handoff)
- [ABM SDR Account Planning](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-marketing/#sdrabm-account-planning)
- [ABM SDR Prospect Discovery]([https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-marketing/#abm-sdr-discoverorg-prospect-discovery])

#### ABM: What does this look like for the SDR organization?

| Team | Q2 FY21 | Q3 FY21 | Q4 FY21 |
| ------ | ------ |------ |------ |
| Account Based Marketing | (in process already) Roll out intent data to acceleration for territory planning <br> <br> Begin serving digital “warm up” through Demandbase 30 days out from accel team landing | Roll out next iteration of the ICP  <br> <br> regional intent dashboards for SDR org |  |
| Acceleration Team |  Align with ABM team to target accounts in our ABM strategy <br> <br> Land in existing territories with geo rep for territory planning and acceleration training | AMER - 800 accounts being worked per campaign (60 day period over 4 sales territories) <br> <br> EMEA - 500 accounts being worked per campaign (60 day period over 8 sales territories) |  |
| Regional SDR | Meeting and IACV goals implemented <br> <br> begin working with acceleration team on accel motion in territory | AMER - 800 accounts being worked per campaign (60 day period over 4 sales territories) <br> <br> EMEA - 500 accounts being worked per campaign (60 day period over 8 sales territories) | Enablement to acceleration function completed by end of Q4 | 

#### SDR ABM Account Handoff

In order to ensure that we are creating a seamless and positive experience for prospects, it is important that we follow the process outlined below for all accounts transitioned into or out of the ABM program.

**Onboarding (Regional SDR transitioning accounts to ABM SDR)**
Regional SDR’s should ensure that the following actions are completed and associated fields are updated before handoff. Once complete, please notify the assigned ABM SDR using Salesforce Chatter.
- Update `Description` (under *Partner Info*)
- Update `Technology Stack` (text field under *Gitlab/Tech Stack Information*)
- Verify `Using CE` (checkbox under *Gitlab/Tech Stack Information*) using version.gitlab.com
- Add known `Active CE Users` (text field under  *Gitlab/Tech Stack Information*) 
- Add working documents, attachments, SDR<>SAL working documents, and any other notes to the account object

If there are questions that come up regarding a specific target account after the handoff has been completed, the ABM SDR should reach out to the prior SDR account owner for input.

**Offboarding (ABM SDR transitioning accounts to Regional SDR)**

When an account is removed from the ABM program, the ABM SDR is required to attach the Account Planning document plus all actionable intelligence uncovered to the account record in Salesforce. It is also the responsibility of the ABM SDR to notify the Regional SDR when all relevant data has been captured and the handoff is complete. This should be done using Salesforce Chatter. 

If there are questions that come up regarding a specific target account after the handoff has been completed, the Regional SDR should reach out to the ABM SDR for input.


#### Account Based SDR Account Planning

ABM SDRs are expected to thoroughly research and understand their target accounts and target verticals. Before targeting any ABM account, the following planning tasks should be completed and documented in Salesforce (where appropriate):

**Uncover general account information**
* How do they refer to themselves and their teams? (review website, press releases, company content / articles, org chart)
* Look at current IT job postings (indeed / create alerts)
* Research recent news / headlines (google search / create alerts)
* Understand organizational structure (DiscoverOrg org charts, general research)
* Look at financials, earnings reports, 10ks (DiscoverOrg, Google search, general research)
* Identify key investment / development areas. (Are they focused on website, mobile app, user experience, migration/cloud initiative, security issues / breaches, cost cutting initiatives, strategic projects, etc.?)

**Review history with GitLab**
* Capture actionable intelligence from the SAL or account owner
* Review all Closed/Lost opportunities 
* Understand current CE usage / GitLab footprint (using version.gitlab.com)
* Look at recent leads, engagement, website activity (using Salesforce and DemandBase)

**Understand intent and buying signals**
* Intent determination (using Salesforce reports plus the weekly update via Demandbase)
* Live intent (via Slack alerts)
* Create job alerts (via indeed / Google)

**Discover the right prospects**
* Import ALL key IT prospects from DiscoverOrg to Salesforce ("VIP", "Influencers", "Users", "Change agents", "Fans", and "Potential Users")
* Identify other key prospects not in DiscoverOrg using LinkedIn Sales Navigator
* Source additional email- and phone- validated prospects on an as needed basis

**Know your vertical(s)**
* Thoroughly review the account based marketing campaign briefs
* Review relevant case studies (GitLab and non-GitLab)
* Research industry thought leaders / events / industry hot topics
* Understand the competitive landscape (Crayon / research)

#### Account Based SDR Workflow
If you have an ABM account, you will need to follow [this](https://docs.google.com/document/d/1ZwP23cIwZ692XcO5MsJl2611wQAjsnNn9J1ZSrfdZVY/edit?usp=sharing) outbound workflow to ensure we are properly measuring and tracking all outbound efforts. There is also an [unfiltered YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrrAseaD0m4ty4oPU8FyKGD) that walks you through the tooling workflow step by step.

#### Account Based SDR DiscoverOrg prospect discovery
Discovering new prospects is an important part of ABM account development. In order to do this in an efficient and organized manner, we recommend using the Target Persona Segments and process listed below.

**Target Segments**

* **VIP**: *CTO, CIO, CSO, CISO, VP of IT, Managing director of IT, Chief Architect, Chief Systems architect, Global Head of DevOps, Head of DevOps, Head of Engineering, Head of Development*
* **Influencer**: *Directors or Managers of software development, software dev, software engineering, engineering, application development, app dev, devops, security, site reliability, site reliability engineer, cloud engineer, cloud architect, cloud services, cloud native app dev, cloud native application development, enterprise architect, principal architect, software architect, data architect, technical architect, application architect, architect, consultant*
* **User**: *Developers, engineers, Q/A, test, business users, interns/students*

`Please note: these should be imported as LEADS, not contacts`

Steps: 
1. Run [SFDC Report Account List](https://gitlab.my.salesforce.com/00O4M000004aAdS) and Export your Tier 3 Account List
2. DiscoverOrg List Match your Tier 3 Accounts and view in Advanced Search
3. Execute Boolean Search for each of the Target Segments in DiscoverOrg
4. Export each search into a CSV that is broken up by Target Segment

Resources:
* [SFDC Report Account List](https://gitlab.my.salesforce.com/00O4M000004aAdS)

#### Account Based Outreach Motion

**1:1 Targeted**
- Goal: Meetings & engagement
- Audience: Existing leads and contacts in salesforce as well as top prospects
- Outreach strategy: Highly personalized
   - use actionable intelligence, SAL insights, Closed/Lost Opportunity reports, historical data, and intent signals to guide 1:1 outreach to individual prospects
   - Use "Triple touch" targeting for top prospects (phone, linkedin, email)
- Suggested Outreach Content: 
   - Business Resilience - [Deliver Better Products Faster](https://app1a.outreach.io/sequences/5665)
   - Business Resilience - [Increase Operational Efficiencies](https://app1a.outreach.io/sequences/5678)
   - Business Resilience - [Reduce Security & Compliance Risk](https://app1a.outreach.io/sequences/5746)
   - Value Driver - [Deliver Better Products Faster](https://app1a.outreach.io/sequences/5815)
   - Value Driver - [Increase Operational Efficiencies](https://app1a.outreach.io/sequences/5816)
   - Value Driver - [Reduce Security & Compliance Risk](https://app1a.outreach.io/sequences/5817)

**Groundswell**
- Goal: Drive traffic to landing pages, track engagement, encourage opt-ins and registrations
- Audience: Influencers & Users, titles will vary from account to account, top candidates should be determined by the SDR who owns the account, SDRs should try to steer clear of targeting buyer personas and top prospects for the Core Conversion play (below)
- Outreach strategy: Personalized
   - Users: More personalization for users with influence. Maximum of 3 / account or business unit being targeted simultaneously
   - Influencers: Highly personalized. Maximum of 3 / account or business unit being targeted simultaneously
   - Promote upcoming events / offers
   - Use engagement to prioritize personalization
- Suggested Outreach Content
   - [Account Based Groundswell sequence](https://app1a.outreach.io/sequences/6054)

**Core Conversion**
- Goal: Expand on interest in trials / evals / core usage, generate meetings to share more about the eval options, generate opt-ins / registrations, track engagement
- Audience: Influencers & users, titles vary from account to account, target lead- developers, architects, engineers, etc.
- Outreach strategy: highly personalized
   - Users: “Your peers are using GitLab to value x, y, z. Learn how you can solve similar challenges and try it for yourself”
   - Influencers: “GitLab is being used at your org to value x, y, z. We’d love to share how your peers are using GitLab today and look at how your team could benefit as well.”
   - Promote upcoming events / offers
   - Use engagement to prioritize personalization
   - Use "Triple touch" targeting for top prospects (phone, linkedin, email)
- Suggested Outreach Content
   - Core Conversion sequence (to be linked Aug 5, 2020)

**Buyer Persona**
- Goal: Meetings, engagement, opt-ins / registrations
- Audience: [GitLab Buyer Personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/buyer-persona/)
- Outreach Strategy: Highly-personalized
   - Persona-based sequences with 1:1 personalization using the associated snippets
   - Promote events, offers, landing pages in outreach
   - Maximum of 3 of each buyer persona / company or business unit
   - No fully-automated sequences
   - Use "Triple touch" targeting for top prospects (phone, linkedin, email)


### Native Geo Coverage (EMEA-only)

The EMEA ABM Acceleration SDRs are directly aligned to "Native Geos" where they are responsible for aligning their outbound efforts to the ABM campaigns and strategy. Each ABM Acceleration SDR in EMEA owns two (2) Native Geos and those assignments can be found in the [Alignment Section](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#alignment) above.

**Native Geo Tactics**
- Top Target Account List development (Top 10 / Top 20)
- ABM account planning
- 1:1 outreach
- Vertical- or Industry- targeting
- Intent data targeting

**Working with Sales & SDR Teams**

ABM Acceleration SDRs are expected to lead the account planning and outbound targeting strategy with the Regional SDRs & Sales teams. Because the territory is shared, it is important to establish open lines of communication with other SDRS and ensure that all parties are on the same page when it comes to target accounts and outbound prospecting strategy. 

**Required Meeting Cadence:** 

Schedule and lead a weekly outbound sync meeting with aligned SALs & SDRs by following the steps below.

1. Schedule a 30 minute weekly meeting for each Native Geo
2. Include an agenda
     - Review target account lists
     - Review outbound strategy
     - Share wins / progress with target accounts
     - Discuss upcoming Meetings & IQMs
     - Discuss upcoming field initiatives in region
3. In the first meeting, create a working agenda document
4. Take detailed notes 
5. Assign ALL action items and include a due date

Acceleration ABM SDRs should participate in initial IQMs and are required to make the Regional Sales and SDR teams aware of all Meetings, Opportunities, and Next Steps. 

### Territory Warm-up Coverage

**Working with Sales & SDR Teams**
For Territory Warm-up coverage, meeting cadence consists of the following:

*  `Initial Coverage Kick-off meeting`: Time - 30 minutes; Discuss tactics, target accounts, protected accounts, events, prospecting strategy, and schedules
*  `Weekly Sync meeting`: Scheduled and led by regional SDR
*  `Coverage Recap meeting`: Time - 30 minutes; Discuss results, account intelligence, actionable insights, and collect feedback
*  `Additional Ad-hoc meetings` as needed 

Acceleration ABM SDRs should participate in initial IQMs and are required to make the Regional Sales and SDR teams aware of all Meetings, Opportunities, and Next Steps. 

**Target Account Identification**

Identifying *known* and *unknown* Target Accounts is an important first step in the Acceleration Coverage Process. To determine the target accounts for a region, the following process should be used:
1.  Identify *known* accounts from Salesforce.com
- `Pull Accounts by *Account Owner*` using [this report](https://gitlab.my.salesforce.com/00O4M000004dmXG)
- `Exclue ALL Customers`
- `Exclude SDR Targeted Accounts`
- `Exclude Accounts with Active Opportunities`

2.  Identify *unknown* accounts from DataFox using [this template](https://app.datafox.com/dynamic-list/5e9e410f3d6bcf0100aa223e/data)
- `2000+ employees for Enterprise, 1000-1999 for Mid-Market`
- `Exclude Government / Public Sector`
- `Filter by Region` (State or Metro)
- `Exclude Existing Accounts` (via SFDC export and DataFox upload)
- `Match Subisidiaries`

3. Combine Salesforce & DataFox target account lists
4. Dedupe & Review
5. Share Target Account List with ABM team for Demandbase appending and targeting
6. List Match in DiscoverOrg (by company domain)
7. Execute saved Boolean searches and append prioritization data to target lists
8. Run CE Usage Reports & Append data
9. Divvy up target accounts 
10. Review final Target Account document with Regional SDR & SAL teams
11. Ensure SDR Targeted Accounts & SAL suppression lists are up-to-date
12. Finalize Target Account List
13. Pull target prospects (by titles & keywords) from DiscoverOrg
14. Submit Issue for Marketing Ops DiscoverOrg Prospect List upload

While we've made every effort to make this process easy to follow and execute, we understand that there are several steps that can be confusing. In order to address those concerns, you can view a [video walkthrough of the process here](https://drive.google.com/open?id=1Byf41XpOa5qWNBhO0v748XbTHbooA9Do).

**Account Scoring & Prioritization**

The next step after identifying all of the Net New target accounts in a region is to prioritize them. The scoring model below should be used to determine the Net New Account Tier which will help guide prioritization and prospecting efforts using the Outbound Prospecting Framework (below).

`A Tier Accounts` - Have at least 4 out of 5 qualifiers below

`B Tier Accounts` - Have 3 out of 5 qualifiers below

`C Tier Accounts` - Have 2 out of 5 qualifiers below

`D Tier Accounts` - Have 1 out of 5 qualifiers below

`F Tier Accounts` - Have 0 out of 5 qualifiers OR zero direct revenue potential in the next 3 years

**Account Scoring Qualifiers:**
*  Current CE Usage
*  250+ employees in IT/TEDD positions
*  Good Fit Industry / Vertical (High Growth, Technology, Financial, Heathcare, Regulated Business)
*  Early Adopters / Innovative IT Shops (Identifiers & Keywords): Kubernetes / Containers, Microservices, Multi-cloud, DevOps, DevSecOps, CICD (and open-source + proprietary tools), SAST / DAST, Digital Transformation
*  Current DevOps Adoption (multiple DevOps roles on staff or hiring for multiple DevOps positions)

**Outbound Prospecting Framework**

| **Tier**        |  **Goal**                 | **Priority Level**      | **Outbound Tactics**      |
| :---------- |  :----------                  | :----------------- | :----------------------|
| A Accounts   |    Conversations, IQMs, MQLs         |      High (60% of focus)      | Hyper-personalized, simultaneous targeting, creative, direct mail, targeted ads, groundswell, events |
| B Accounts   |  Conversations, IQMs, MQLs           |      High (30% of focus)      | Hyper-personalized, simultaneous targeting, creative, direct mail, targeted ads, groundswell |
| C & D Accounts |  Conversations, MQLs               | Low  (< 10% of focus)         | Automated role- / persona-based outreach, groundswell |
| F Accounts |  Eliminate from SDR target lists   | Low (< 2% of focus)     | Do not target or attempt to qualify |

## Acceleration Programs

The top priority for the Acceleration team is providing a great experience for the individuals that we engage with. In an effort to better understand and reach prospects, the team is encouraged to iterate and continuously improve how we tell the GitLab story. "Acceleration Programs" are MVCs that are tested, measured, and then expanded as success is proven. 

1.  The first step in launching a project is to share the idea in a team call. 
2.  After the team has an opportunity to provide feedback, a tracking and measurement plan should be created and documented. 
3.  Once that is complete, the tactic or strategy should be incorporated into the individuals' outbound process and measured over a period of four to eight weeks.
4.  During the testing period, measurement is required and results should be documented weekly. 

If, after analyzing results, it appears that the tactic will generate at least a 1% lift if incorporated into the [Outbound SDR Workflow](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#outbound-workflow), an issue should be created with the :acceleration programs: label and a rollout plan should be created.

Active Acceleration Programs can be viewed on our [Global Acceleration issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1333112?scope=all&utf8=%E2%9C%93&state=opened).

## Helpful definitions

**Territory Warm Up** - The concept of focusing on a sales region for a period of time in an effort to generate Qualified Meetings, SAOs, and Pipeline. This is an important strategy for developing *new* and *underperforming* regions while generating opportunities for new SALs and AEs. 

**Hyper-personalization** - This is the concept of combining real-time data extracted from multiple sources to create outreach that resonates with prospects on an individual level. The desired outcome is to establish relevance with a prospect as a first step towards a conversation.

**VIP (prospect)** - A Very important top officer, exeutive buyer, C-level prospect, or important influencer. For these individuals, hyper-personization is required. Examples: CTO, CIO, CSIO, C-level, IT Business unit leads, VPs, strategic project leaders.

**Influencer (prospect)** - An individual prospect that is suspected to be involved with IT decision-making, tooling, teams, roadmap, strategic projects, and/or budgets. Examples: Director or Manager of DevOps / Engineering / Cloud / Security, Enterprise Architects, IT buyers, SysAdmins, purchasing managers, and product owners.

**User (prospect)** - A prospect that has limited influence within an IT organization. Examples: Developers, Engineers, QA, consultants, and business users.

**Groundswell** - An outbound strategy focused on filling the top of the funnel by generating engagement, opt-ins, MQLs, and uncovering intent signals. This strategy typcially incorprates more automation than other more direct outbound prospecting tactics. The strategy should be used with lower-level prospects and lower tier accounts.

**Value Stream** - An outbound "play" that is aligned to a prospects' role, title, persona, industry, vertical, current technology stack, desired business outcomes, assumed pain points, or keywords to 1) establish relevance 2) create value and 3) drive engagement and conversations.

**Warm Calling** - The method used to strategically incorporate phone calls and voicemails into an outbound prospecting workflow. The idea is optimize outbound productivity by only using the phone when targeting *engaged*, *validated*, and/or *VIP* prospects. 
