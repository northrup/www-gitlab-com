---
layout: handbook-page-toc
title: Building High Performing Teams
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

One of the greatest challenges for leaders is to build and develop high performing teams. This page explores what it means to have a high performing team at GitLab. It will also explore a tactical roadmap called the Drexler-Sibbet Team Performance Model that managers can reference to build high performing teams. 

## What Makes a High Performing Team at GitLab

Through various interviews with executive leadership and managers at GitLab, we have identified a series of skills, behaviors, and attributes of high performing teams at GitLab. Many of these points are in alignment with our values. To operate as a team in a remote environment, trust needs to be at the center of the formation of the team. Additional skills, behaviors, and attributes of high performing teams include: 

- Handbook first approach
- Shared desire to grow others
- High Level of self-awareness
- Living our values
- Strong sense of ownership by individual team members
- Ego's are put into check
- Ability to see viewpoints from different perspectives
- Bias towards action
- Asynchronous communication practices
- Iteration, breaking down complex information into digestible parts 
- Trust-based relationships where everyone is open to provide feedback

## Strategies to Improve Building High Performing Teams 

The [Drexler-Sibbet Team Performance Model](https://www.kaizenko.com/drexler-sibbet-team-performance-model/) is an excellent tool to help build high performing teams at GitLab. The model provides a roadmap for a team and a common language. It is a simplified description of how a team works together that highlights the most important things the team needs to focus on to reach high performance. At GitLab, we can use it as a frame of reference to developing high performing teams. It can help Managers ensure new and existing team members know the mission and direction of the team by the following: 
- To form your team
- To guide what your team does
- To monitor how well your team is doing
- To diagnose where your team may be struggling or identify the keys to your team's success

Let's explore the stages in more detail and strategies you can implement as a Manager in building a high performing team. 

**7 Stages to developing high performing teams:**
1. **Orientation** - Why are we here? Team members need to see a sense of team identity and how individual team members fit in. 
2. **Trust Building** - Who are you? Team members share mutual regard for each other and are open and supportive of trust-based relationships. 
3. **Goal Clarification** - What are we doing? Assumptions are made clear; individual assumptions are made known with a clear vision of the end state. 
4. **Commitment** - How will we do it? Team members understand how it will make decisions and do the work. 
5. **Implementation** - Who does what, when, where? Team members have a sense of clarity and can operate effectively due to the alignment of shared goals.
6. **High-Performance** - Wow! The team is accomplishing more than it expected. The team has taken off, creativity is fostered and goals are surpassed. 
7. **Renewal** - Why continue? The team is given recognition and celebrates achievements of individuals that produce valuable work. Reflect on lessons learned and reassess for the future.

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTiO3RkrjOgOJ6tqvEEg7BXIP6wJrlmazKHD-IUTDXTnGmDC3m4h7N0eXG1kUWoHmSGArkcsHOo1ln0/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### Stage 1 Orientation - Why am I here?

Why am I here? Team needs to move from complete freedom to a set of “boundaries and constraints” that are needed to operate. The first step is to understand the purpose of the team. 
- The first step of orientation is to understand the purpose of the team. Why does the team exist and what is expected of it? 
- Team members need to see a sense of Team Identity in relation to the team. 
- Membership has to do with understanding how you fit in on the team. New team members may be asking - “How can I make a difference on this team?” 
- Unresolved orientation stage can include disorientation, uncertainty, and fear and can prevent trust-building. 

**Questions that need to be answered in Stage 1:** 

- Why does the team exist?
- What is expected of the team?
- Do I belong here?
- Is the work valuable and meaningful to me?
- How will my skill be used?
- How can I best contribute?

<details>
  <summary markdown='span'>
    Stage 1 Diagram
  </summary>

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRHRyishV7cc9k78hA7coEyYw0udvNBIWOgZZtMCxmzwPBPU_0_7b6BizAxH_1qocAZHhGhyWxuOcCt/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

</details>

**Stage 1 Manager Tasks:**
1. Document the team’s purpose/mission
2. Review the model with new and existing team members
3. Set up a 1:1 with a new team member as soon as they start to orientate them on the team’s missions and goals 
4. Set clear expectations with your team from the start and ask them to identify where your team is in the model
5. Assign a buddy or mentor to each new team member to help them acclimate to the team

<details>
  <summary markdown='span'>
    Sage 1 Manager Tasks Diagram
  </summary>

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSiNEAtjZlzmTqNCNCrN81l2lTEg-VpRgVNC2UGyyprbjwcng_f4UDI3MstrgEkfU3NY-q9qJUy0BT0/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

</details>

### Stage 2 Trust Building - Who are you?

Who are you? Team members share a mutual regard - the climate is positive, people are respectful and supportive. There is a willingness to be forthright, open, and free!
- Willingness to be forthright, open, and free in working with team members. 
- Team members are direct and upfront during interactions where team members understand each other’s intentions.
- Team members can be upfront when they understand other’s interpersonal needs and work styles.
- Unresolved trust concerns include caution, mistrust, and facade.

**Questions that need to be answered in Stage 2:** 

- What do others expect of me?
- Can others be relied upon? 
- Are others competent?
- What drives other team member's behavior?
- What are other team member's interpersonal needs and communication styles?
- Do team members have hidden agendas?

**Stage 2 Manager Tasks:**
1. Have each team member share where they see themselves in one to five years
2. Conduct a group assessment (e.g. Myers Brigg, Gallup StrengthsFinder, [Social Styles](https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/)) 
3. Have each team member reveal their pet peeves and unique attributes
4. Have each team member complete a skills and knowledge profile and share it with the team. Encourage team members to utilize each other’s skills and knowledge. If someone wants to build skills in a specific area, encourage peer mentoring
5. Establish team ground rules and communication protocol
6. Review the model with the team quarterly and track progress

### Stage 3 Goal Clarification - What are we doing?

What are we doing? Explicit assumptions are made clear; individual assumptions are made known. A clear vision of the end state and what the team is working towards. 
- Assumptions are made clear; individual assumptions are made known where values are stated explicitly.
- Key is to have clear integrated goals that team members agree to support; long term goals and short term milestones and objectives should be formed to guide day-to-day work.
- A clear vision of the end state the team is working toward; jointly defining the vision together.
- Unresolved goal clarification concerns include apathy, skepticism, and irrelevant competition.

**Questions that need to be answered in Stage 3:** 

- How do we picture success now and in the future?
- What are the options and alternatives for achieving that success?
- What are our goals?
- What assumptions are we making about our mission and our work?
- Exactly what are we to do? 

**Stage 3 Manager Tasks:**

1. Collectively set a team vision that inspires the team members
2. Collectively develop team goals in support of the mission and vision. Make sure the goals are SMART (specific, measurable, attainable, realistic, time-sensitive). Document the goals and periodically assess. 
3. During 1:1s, discuss how the individual team member’s performance ties to the team goals. Ask the team member for their assessment of how their contributions tie to the team.  

### Stage 4 Commitment - How will we do it?

How will we do it? The team manages its resources well to make use of what they have to do the job. The team needs to understand how it will make decisions; how much control and influence are shared.
- Decisions are made about exactly how time, resource allocation, and team members will be managed.
- Make sure team members are clear on assigned roles and goals. 
- The team needs to understand how it will make decisions, how much control and influence are shared.
- Commitment to allocated resources needed to do the job.
- Unresolved commitment concerns include dependence and resistance.

**Questions that need to be answered in Stage 4:** 

- How do we picture success now and in the future?
- What are the options and alternatives for achieving that success?
- What are our goals?
- What assumptions are we making about our mission and our work?
- Exactly what are we to do? 

**Stage 4 Manager Tasks:**

1. Collectively identify all team decision points. Specify who is responsible for each decision. 
2. Review, document, and communicate the responsibilities
3. Define and document individual roles. Discuss assumptions that team members have about roles. Address differences in assumptions. 

### Stage 5 Implementation - Who does what, when, where?

Who does what, when, and where? There is clarity so that the team can take off and move towards creativity. Clear processes in place that blend with the team. 
- This stage indicates that the team has all of the boundaries it needs to begin operating effectively. 
- Clarity so that the team can take off and move with freedom and creativity since parameters are known.
- Activities of team members blend to create a smooth alignment.
- Disciplined execution leads to efficient work operations and clear processes to hit deadlines.
- Unresolved implementation concerns include conflict and confusion, nonalignment, and missed deadlines.

**Questions that need to be answered in Stage 5:** 

- Does the team have clear enough procedures?
- How will the team track progress?
- What is the best sequence of activities?
- How do I make the procedures clear to everyone?
- How does the team communicate and coordinate with each other?
- How does the team measure performance?

**Stage 5 Manager Tasks:**
1. Document and communicate Handbook first operating procedures.
2. Document and communicate team metrics and targets. 
3. Clarify and communicate individual targets. Hold individuals accountable for expectations and targets
4. Utilize GitLab schedules and trackers and assign someone to own components of the plans. 
5. Periodically solicit input from senior leaders on the team’s performance and execution. Share the feedback with the team. 

### Stage 6 High Performance - Wow!

The team accomplishes more than it is expected to. The team has taken off and creativity is fostered to reach new levels of productivity and results. 
- WOW!!! The team accomplished more than it expected to. The team has taken off and boundaries are lifted. Creativity is fostered. The team is agile, flexible, and can adjust goals as needed.
- There is a high sense of trust. Team members can do what they need to do and be assured that other tasks are getting done
- Achieve more working together than you would have achieved as individuals through synergy.
- Unresolved high-performance concerns include overload and disharmony.

**Questions that need to be answered in Stage 6:** 
- Does the team want to be good or great?
- How creative can the team be?
- How do team members best use the resources of the team?
- How does the team support one another? 

**Stage 6 Manager Tasks:**
1. Define what high performance would like for your team. Hold a quarterly retrospective.  
    Ask:
        - What would it mean if we are surpassing results?
        - What would be doing differently that we are doing today?
        - What can we do better in the future?
2. Hold team meetings to examine what else is possible. 
    Ask: 
        - What else can we achieve and accomplish that would be added value to GitLab?
3. Conduct team after-action reviews and debriefs. Reflect on what the team learned and what the team could have done differently. 

### Stage 7 Renewal - Why Continue? 

Why continue? It is time to give recognition to or celebrate the achievement of individuals that produce valuable work and provide results - take joy in what the team does. 

- Give recognition to or celebrate achievements of individuals that produce valuable work. Take joy in what the team does. 
- The team should demonstrate change mastery, confront new challenges, and deal with shifting conditions.
- The team needs to prepare for and manage leadership transitions and continue to assess individual aspirations and interests.
- Find ways to keep themselves fresh and energetic by reflecting on learning and opportunities to improve.
- Unresolved renewal may include boredom and burnout.

**Questions that need to be answered in Stage 7:** 
- Does the work fit with team members interests and skills?
- Are team members still getting satisfaction from the work?
- Is the team getting the recognition it deserves? 
- Does the team celebrate our successes enough?
- Is the team burning out?
- What changes can the team anticipate? How is the team preparing for them?
- How can the team continue to improve performance? 

**Stage 7 Manager Tasks:**
1. Quarterly conduct a meeting and have each team member come with at least one idea on how the team can improve its performance. Discuss the ideas as a team and vote to identify at least one idea for implementation. 
2. Conduct development discussions with each individual regularly. Inquire about their level of engagement. Ask them what else they would like to learn and explore how those developmental opportunities can be integrated into the role. 
3. Review team and individual accomplishments quarterly. Take a coaching approach to the discussion. 
4. Identify opportunities for executive leadership to recognize the team.
5. Write personal thank-you notes in the Slack channel. 
6. Identify opportunities to rotate team members to other teams. Allow team members to participate in developmental experiences. 

### Summary
The Drexler-Sibbet Team Performance Model is an excellent tool to building and maintaining high performing teams at GitLab. As a Manager, use some of the tactical steps outlined on this page to ensure your team has a common understanding of the direction of the team. Your team will be on its way to building a high performing team!

## Additional Resources
- Read:[Building and Leading High-Performing Remote Teams](https://www.shrm.org/resourcesandtools/hr-topics/technology/pages/building-leading-high-performing-remote-teams.aspx) - SHRM
- Read:[What Google Learned From Building the Perfect Team](https://www.nytimes.com/2016/02/28/magazine/what-google-learned-from-its-quest-to-build-the-perfect-team.html) - NYTimes
- Watch: [How Google Builds the Perfect Team](https://www.youtube.com/watch?v=v2PaZ8Nl2T4) - Tech Insider
- Watch: [What Makes the Highest Performing Teams in the World](https://www.youtube.com/watch?v=zP9jpxitfb4) - Simon Sinek 
