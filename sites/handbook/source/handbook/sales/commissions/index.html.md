---
layout: handbook-page-toc
title: "Commissions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
## Quick Reference Guide

| **GETTING STARTED** | **QUOTAS** | **COMMISSION PLANS** |
| :------ | :------ | :------ |
|[**FY21 Sales Compensation Plan**](https://about.gitlab.com/handbook/finance/sales-comp-plan/)<br><br>[**FY21 Commission Program**](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g153a2ed090_0_63)| [**Quotas Overview**](/handbook/sales/commissions/#quotas-overview)<br><br>[**Quota Components**](https://about.gitlab.com/handbook/sales/commissions/#quota-components)<br><br>[**Sales Rep IACV Quota Ramp**](/handbook/sales/commissions/#sales-rep-iacv-quota-ramp)<br><br>[**Seasonality Assumptions**](/handbook/sales/commissions/#seasonality-assumptions) | [**Commissions Overview**](/handbook/sales/commissions/#commissions-overview)<br><br>[**FY21 Commission Program Terminology**](/handbook/sales/commissions/#fy21-commission-program-terminology)<br><br>[**Commissions Rules**](/handbook/sales/commissions/#commissions-rules)<br><br>[**Commission Hold and Releases**](/handbook/sales/commissions/#hold-and-releases)<br><br>[**Participant Schedules**](/handbook/sales/commissions/#participant-schedules)<br><br>[**Opportunity Splits**](https://about.gitlab.com/handbook/sales/commissions/#opportunity-splits)

## Quotas Overview

All employees with variable compensation plans will be assigned a quota that supports their
assigned territory/region shortly after the beginning of each fiscal year or when they enter into their role. Quota assignment and quota ramp (including any mid-year adjustments) are determined by Sales Leadership. Sales Leadership reserves the right to make any adjustments to quotas. 

The quota coverage types are:
1. **Native Quota Rep (NQR)** - individual owns direct quota for their assigned territory.
2. **Overlay Quota Rep (OQR)** - individual supports an assigned territory that is currently owned by a NQR.

### Quota Components

Can be made of various measurements, but typically by IACV at the Teritory, Region or Segment levels.

The current Commission Plan [presentation](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g153a2ed090_0_63) outlines the key components in detail.  

### Sales Rep IACV Quota Ramp

For all enterprise native-quota carrying salespeople, we assume around a twelve (12) month ramp before a rep is fully productive. We have the following ramp up schedule and measure quota performance based on this schedule:

#### Enterprise
* First Quarter Hired: 0% contribution of quarterly target
* Second Quarter Hired: 30% contribution of quarterly target
* Third Quarter Hired: 65% contribution of quarterly target
* Fourth Quarter Hired: 90% contribution of quarterly target

#### Mid Market
For all Mid Market native-quota carrying salespeople, we assume around a six (6) month ramp before a rep is fully productive. We have the following ramp up schedule and measure quota performance based on this schedule:
* First Quarter Hired: 25% contribution of quarterly target
* Second Quarter Hired: 50% contribution of quarterly target
* Third Quarter Hired: 100% contribution of quarterly target

#### Seasonality Assumptions
We also factor in seasonality into our calculations. We expect most of our business to close in the second half of the year. Our seasonality assumptions are as follows for the private sector:

* First Fiscal Quarter: 17%
* Second Fiscal Quarter: 23%
* Third Fiscal Quarter: 28%
* Fourth Fiscal Quarter: 32%

Given the fiscal period for public sector, our seasonality assumptions differ:

* First Fiscal Quarter: 15%
* Second Fiscal Quarter: 20%
* Third Fiscal Quarter: 55%
* Fourth Fiscal Quarter: 10%

#### Quotas Proration
1. Native Quota-Carrying Reps: prorated quotas are based on the seasonality assumptions as demonstrated above.
2. Overlay Quota Reps: proration is based off the number of days employed in the given period. Your prorated quota would be calculated as follows: (Quota / (Period End Date - Period Start Date)) * (Period End Date - Hire Date). For example, if the regional quota is $10,000,000 for the year and the start date is 4/1/2019, your quota will be calculated as ($10,000,000 / (2020-01-31 - 2019-02-01)) * (2020-01-31 - 2019-04-01) = $8,379,120.88.

## Commissions Overview
Most commission plans are paid monthly; however there are some plans that are paid on a quarterly basis. Most commissions plans are calculated in CaptivateIQ. 

### Commission Rates
You can find the definitions of **Base Commission Rate (BCR)** and **Super Commission Rates (SCR)** in the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/#definitions) page under the Definitions section.

## FY21 Commission Program Terminology

### Base Commission Rate (BCR)
Base Commission Rate (BCR) is the commission rate applicable to all salespeople that are on a commission plan. BCR is calculated as Annual OTI (On Target Incentive) / Full and/or Normal Quota and is the base rate to which multipliers are applied to arrive at the accelerated rates. 

### Super Commission Rate (SCR)
New salespeople who join after the fiscal year may be eligible for a Super Commission Rate (SCR) which is calculated as Prorated OTI / Prorated Quota. OTI is prorated based on their start date while their Quota is prorated based on ramp and seasonality assumptions. 
[Example of SCR](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g6ee04e54a5_0_47)

David Hong, VP of Field Operations, explains how the Super Commission Rate works:
<!-- blank line -->
<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/2RVm2OmDuOo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
<!-- blank line -->

### Channel Neutral Compensation
FY21 commissions will be channel neutral for all deals thru partners (including Distributors, if applicable), which means standard partner discounts are credited back to the salesperson and must follow [order processing procedures](https://about.gitlab.com/handbook/business-ops/order-processing/) to correctly flag partner attribution. Total IACV on the deal after all discounts will count towards quota credit, but the channel neutral amount does not qualify for quota credit and only pays out for compensation at BCR. See Channel Neutral section referenced in the [FY21 Commission Plan presentation](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g7da31a6494_6_0)

### Multi Year Commissions
Salespeople are eligible to earn multi year commissions if the opportunity term is more than 12 months and the customer prepays the entire Total Contract Value (TCV). First year IACV will be paid at BCR or SCR , if applicable and will be counted towards quota credit. Outyear IACV will be paid at 50% of BCR and will not be counted towards quota credit. 


## Commissions Rules
The following rules will apply for the various roles within the organization:

| **Role** | **Rules**  | **Explanation** |
| ------ | ------ | ------ |
| Strategic Account Leader, <br>Mid Market Account Executive | Is the Opportunity Owner | Opportunity owner will receive the commission |
| SMB Customer Advocate | Account Owner is SMB AND Account Owner is on SMB AE’s Regional Team | Account owner is an SMB team member AND the Account Owner is on regional team, SMB AE will receive credit |
| Area Sales Manager (ASM) | Opportunity Owner is subordinate AND<br>Account Owner is on Regional Team | If the Opportunity owner is a subordinate AND the Account Owner is on the ASM's regional team, the ASM will receive credit |
| Regional Director (RD) | Account Owner is your subordinate | The opportunity owner may be on a different regional team, but if the Account Owner is on the RD's team, the RD will receive credit. It is possible to override if the RD of the opportunity owner requests credit for the deal |
| Vice President (Sales) | Account Owner is your subordinate | The opportunity owner may be on a different Regional team, but if the Account Owner is on the VP's team, the VP will receive credit |
| Inside Sales Representatives - Public Sector (ISR) | Sub-Industry | The opportunity owner may be anyone on the PubSec team so long as the sub-industry is the one supported by the ISR |
| Solutions Architects (SA) | Opportunity Owner is on your regional team | The opportunity owner must be on the SA's team for them to receive credit |
| Technical Account Manager (TAM) | Opportunity Owner is on your regional team | The opportunity owner must be on the TAM's team for them to receive credit |

## Hold and Releases
Commissions are paid when the opportunity is invoiced in full. If the opportunity is not invoiced in full no commissions will be paid. These commissions will be held in the Commission system and will be released by the billing team when invoiced in full. In case of a special situation where the commissions need to be released even though not invoiced in full we would need written approval from the CRO and CFO to release those commissions.

## Participant Schedules
Within the first few weeks of your employment, you should receive your participant schedule. This document will provide details on your plan components, territory, plan start and end dates, payout frequency, base and super (if applicable) commission rates, prorated and full quotas, and variable payouts per component. 

## Opportunity Splits
Credit splits are allowed at an opportunity level and can be requested by native-quota carrying salespeople. Opportunity splits must be requested before any work is shared on an opportunity and approved prior to the closed won date.
* Follow [Opportunity Split approval process](https://about.gitlab.com/handbook/sales/#opportunity-splits)
* On approval opportunity split crediting and commissions will be automatically calculated in CaptivateIQ commissions system

## Sales Representation Letter 

### Introduction

As part of SOX 404 control environment all quota carrying reps need to certify if there were any deviations to the handbook policy with respect to quote approval and contract management process. Reps should read and acknowledge the letter and report any exceptions.

### Goals of certification

The main aim of this rep certification letter is to identify: 

* Any form of communication with a customer that would modify or supersede the terms of an existing subscription and / or a subscription agreement that has not yet been documented or signed. 
  
    These arrangements typically result from efforts to: remediate a performance issue, secure future business, obtain a customer reference, retain or improve customer satisfaction, avoid a legal matter or close a deal. Examples of benefits offered to customers could include the following: free or discounted products or services, write-off of a previously recorded account receivable balance, ability of a distributor to re-sell a license to non-intended end-users, extended warranty rights, offer to return products not sold for full refund outside of policies, payment terms that deviate from GitLab’s standard practice or cash refunds.

### Sales Representation Letter Certifies:

1.  All written or unwritten commitments to customers are documented in the Agreement, Order Form, PO or other written agreement signed by the Customer and authorized signatory at Gitlab.  If commitments are not included in customer agreements then they have been documented in a GitLab issue in which the CEO, CFO or CRO has been tagged.

2. All customer contract files (including purchase orders, contracts, letter agreements, sales offers, amendments, and any other correspondence) are complete, properly signed by each party, and the appropriate contract record in Salesforce is filled in so it is easily accessible.

3. All indirect sales (e.g. those through a partner/distributor/ reseller) are supported by a bona fide end-user purchase order or contract.

4. All purchase orders and/or contracts with customers were fully executed by an appropriate customer employee and authorized GitLab employee, if required by local business practice, on or before the delivery date of such contracts.

5. All side arrangements (if any) with customers have been approved by a GitLab executive with authority to make such arrangements and has subsequently been disclosed to the GitLab Revenue Recognition Team (“Revenue Team” controller, senior technical accounting manager, the Principal Accounting Officer, or CFO) in writing.

6. The Company’s customers have not a) been granted any rights to return products;               b) receive credits for products or services delivered or receive additional products or services that are not specifically included in the agreements or amendments with such customers, or c) transfer of licenses or delivery of the software to an unauthorized end-user. 

7. We did not communicate, directly or indirectly, to any customer that they are free to activate and use the license subscription prior to the subscription start date agreed in the executed order form. Further, we have communicated any knowledge of such unauthorized use to the Finance team.

8. All agreements with unfulfilled performance guarantees or remaining acceptance provisions have been disclosed in the customer agreement or amendments and to the Revenue Team.

9. Multiple agreements with a single customer meeting any of the following criteria have been disclosed to the Revenue Team: agreements were executed within a short time frame (i.e. within 3 months) of each other, the contract terms are interrelated (i.e. payment terms under one agreement are linked to the performance of another), negotiations of the contracts were conducted jointly.

10. The Company has not granted payment terms that deviate from GitLab’s standard practice under any customer sales agreements that are not specifically disclosed in the customer agreement or amendments.

11. All customer arrangements for which payment is contingent on the customer’s obtaining financing from an outside funding source have been disclosed in the customer agreement.

12. There are no known credit memos to be recorded/issued subsequent to the end of the quarter noted above that relate to products or services delivered during the quarter or prior, which would reverse revenue recorded for that arrangement equal to or in excess of 5% of the deal value.

13. Please confirm if there were any changes to quotes after approval or if quotes are sent to a customer and/or not reviewed by Sales operations or Deal desk , please disclose.

14. I personally questioned each of my direct reports (if any) with respect to the representations noted above and am not aware of any deviations not already disclosed.

15. During the Quarter, I have not engaged in any activity, nor have I become aware of any other GitLab employee or employee of any GitLab subsidiary engaging in any activity, which violates the Company’s Code of Business Conduct (https://about.gitlab.com/handbook/people-operations/code-of-conduct/), including any activity in violation of the Foreign Corrupt Practices Act - which prohibits any payments to foreign (in jurisdictions outside of the United States) officials for the purpose of obtaining or keeping business. 

16. Nothing has come to my attention from previous quarter sales activities that have changed or been disclosed to me that I was not aware of previously, but now am aware of.

### Sales Rep Certification Process

Rep letters will be sent quarterly by the Internal Audit Team. Reps are required to acknowledge the certification letter.

### Sales Rep Certification Process Timeline

| **Activity** | **Timeline** |  
| ------ | ------ |  
| Roll out certificates by Internal Audit | 4th day of the subsequent month after the quarter-end |  
| Receive responses by | 8th day of the subsequent month after the quarter-end |  
| Internal Audit to collate all the information and communicate to Principal Accounting Officer | 12th day of the subsequent month after the quarter-end |  
| Principal Accounting Officer and Chief Financial Officer to review  the responses and  to communicate to audit committee | 14th day of the subsequent month after the quarter-end |  

* If any of the timelines above fall under a public holiday then the previous working day will be a due date for the respective activity.

### Request for Quota Relief
1.  The initial request for quota relief shall be made from the individual to their direct manager with sufficient supporting justification for the request.  
1.  If approved by their Manager and Departmental VP; the Departmental VP submits the request to sales-comp@gitlab.com for review by Commissions Team and eventually by CRO.
1.  Final ruling will be communicated back to Departmental VP with details that may include retroactive adjustments and effective date. 

## FY21 SPIFFS (Sales Performance Incentive Funding Formula)

1. To promote healthy competitions and drive the right behaviors, Sales leadership could provide a short-term SPIFF (subject to approval via the [Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix))
2. Sales leadership needs to request approvals via an issue that contains the following details:
* Purpose of SPIFF
* Duration of SPIFF
* Criteria to be eligible for SPIFF
* Total # of eligible winners
* Total SPIFF expenses

### FY21 Professional Services Spiff (v2)
**Effective 2020-06-15 (replacing the prior SPIFF) [FY21 PS SPIFF](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g88c7cba71f_10_1)**


##### PS SPIFF Tracking Process: 
1. Standard Professional Services Deal Process: Follow standard [Professional Services deal process](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/)
2. Primary TAM must be identified on the Account record to receive credit
3. Assigning Solution Architect to Opportunity: The following teams can edit the "Primary Solution Architect Field" on the related Opportunity 
    * Solution Architects
    * Solution Architect Managers
    * Ops (SalesOps, SalesSystems)
    * CS Leadership
    * *Note: Once the opportunity is closed, the field is no longer editable outside of Sales Support/Sales Ops.*
4. Professional Services Compensation:
    * SFDC will capture the PS $ amount in the field named "Comp Pro Serv Value" on the license related opp
    * This will capture both pre-existing SKUs AND associated Professional Services Opportunities for an SOW
5. [SFDC Dashboard](https://gitlab.my.salesforce.com/01Z4M000000slWT) to track PS Open pipe and won deals.   Note "Comp Pro Serv Value" on Opportunity will reflect amount considered for compensation.

