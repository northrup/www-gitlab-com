require 'countries'

module Gitlab
  module Homepage
    class Team
      class Member
        include Comparable
        attr_reader :assignments, :domain_expertise

        def self.normalize_country(country)
          case country
          when 'USA', 'US'
            'United States'
          when 'The Netherlands'
            'Netherlands'
          when 'UK', 'GB', 'Great Britain'
            'United Kingdom'
          when 'NZ'
            'New Zealand'
          else
            country
          end
        end

        def self.all!
          @members ||= YAML.load_file(File.expand_path('../../data/team.yml', __dir__))

          @titles ||= {}.tap do |hash|
            @members.each do |member|
              hash[member['slug']] = member['role'] if member['slug']
            end
          end

          @members.map do |data|
            reports_to = data['reports_to']
            data['reports_to_title'] = @titles[reports_to] if reports_to
            new(data).tap { |member| yield member if block_given? }
          end
        end

        def self.no_vacancies
          @no_vacancies ||= all!.reject { |member| member.name == 'New Vacancy' }
        end

        def initialize(data)
          @data = data
          @assignments = []
          @domain_expertise = []
        end

        def valid_start_date?
          start_date.nil? || start_date.is_a?(Date)
        end

        def <=>(other)
          [vacancy_for_sort, start_date_for_sort] <=> [other.vacancy_for_sort, other.start_date_for_sort]
        end

        def ==(other)
          slug == other.slug
        end
        alias_method :eql?, :==

        def hash
          [self.class, slug].hash
        end

        # This needs to match the implementation of the anchor key in OrgChart#build_json_data
        def anchor
          gitlab || slug
        end

        def username
          @data.fetch('gitlab')
        end

        def involved?(project)
          project_roles.has_key?(project.key)
        end

        def text_role
          @data.fetch('role', '').gsub(/<[^>]+>/, '')
        end

        def project_roles
          @project_roles ||= @data.fetch('projects', {}).map { |project, roles| [project, Array(roles)] }.to_h
        end

        def assign(project)
          project_roles[project.key].each do |role|
            @assignments << Team::Assignment.new(self, project, role)
          end
        end

        def departments
          @departments ||= @data.fetch('departments', [])
        end

        def country_normalized
          self.class.normalize_country(@data['country'])
        end

        def country_info
          @country_info ||= ISO3166::Country.find_country_by_name(country_normalized)
        end

        def start_date_for_sort
          start_date.nil? ? Date.today : start_date
        end

        def vacancy_for_sort
          type == 'vacancy' ? 1 : 0
        end

        def load_domain_expertise
          member_expertise = @data.fetch('domain_expertise', [])

          return unless member_expertise.any?

          Team::DomainExpertise.all!.each do |domain|
            unless @domain_expertise.any? { |d| d.key == domain.key }
              @domain_expertise << domain if member_expertise.include?(domain.key)
            end
          end
        end

        ##
        # Middeman Data File objects compatibility
        #
        def method_missing(name, *args, &block) # rubocop:disable Style/MethodMissingSuper
          @data[name.to_s]
        end

        def respond_to_missing?(method_name, include_private = false)
          @data.include?(method_name.to_s) || super
        end
      end
    end
  end
end
