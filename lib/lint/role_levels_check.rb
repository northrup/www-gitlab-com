require 'yaml'

module Lint
  class RoleLevelsCheck
    REQUIRED_KEYS = %w[factor grade title type].freeze

    def run
      log "Running role_levels.yml and job_families.yml checker"
      role_level_messages = check_role_levels
      job_family_messages = check_job_families

      if role_level_messages.empty? && job_family_messages.empty?
        log "SUCCESS: the role_levels.yml and job_families.yml file are valid"
      else
        error_message = "ERROR: role_levels.yml and/or job_families.yml file contain invalid data"
        log "#{error_message}:\n"
        (role_level_messages | job_family_messages).each_with_index do |msg, i|
          log "#{i + 1}. #{msg}"
        end
        log "\n"
        exit 1
      end
    end

    private

    def role_levels
      YAML.load_file(File.expand_path('../../data/role_levels.yml', __dir__))
    end

    def check_role_levels
      messages = []
      # check if there's always a `is_default` set
      # check if for all the levels the required keys are set
      role_levels.each do |role_title, levels|
        messages << "In role_levels.yml: `is_default` is missing for #{role_title}." if levels.none? { |level| level['is_default'] }
        messages << "In role_levels.yml: not all fields are set for #{role_title}." if levels.any? { |level| !level_has_required_keys?(level) }
      end

      messages
    end

    def job_families
      YAML.load_file(File.expand_path('../../data/job_families.yml', __dir__))
    end

    def check_job_families
      messages = []
      # check if the job_families always refer to an existing level
      all_levels = role_levels.keys
      job_families.each do |job_family|
        next if all_levels.include? job_family['levels']

        messages << "In job_families.yml: the level #{job_family['levels']} for #{job_family['title']} does not exist in `role_levels.yml`."
      end

      messages
    end

    def level_has_required_keys?(level)
      level.delete('is_default')
      level.keys.sort == REQUIRED_KEYS.sort
    end

    def log(msg)
      puts msg
    end
  end
end
