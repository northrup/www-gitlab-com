# Steps for engineering managers on each team to do before the engineering-wide retrospective
1. Please have a retrospective with your team following the guidelines outlined in the [handbook](https://about.gitlab.com/handbook/engineering/management/team-retrospectives/).  
1. After the retrospective is complete, please choose a subset some of your most interesting learnings to share company-wide in the [retro doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit).  Please try to group by *topic* rather than by *team*, as suggested in #3416.
1. In this document, if the item would be of interest to a number of teams in engineering, please indicate that it should be verbalized by adding the text *verablize* at the beginning of the text.
1. For items which have didn't go well, create an issue to address.  In the case where a manager feels an issue can/should not be created, please include that in the what went wrong section.
1. If there are action items in the document for your team from the previous retrospective please provide an update on them in the document.  They will be verablized during the meeting.
1. Add a checkbox in the table below when your retrospective is done and when the document is updated.

Please do all of these steps *at least* one day before the engineering-wide retrospective.

# Steps for engineering managers during the engineering-wide retrospective Zoom session
1. Be prepared to verbalize any items for your team.  Note that the meeting organizer may add additional items to verbalize, so please review the document in advance of the meeting for any additions for your team.
2. If you can't make the meeting and there is an item for you to verbalize, please ask someone else on your team to attend to do so.

| Team                | Eng Manager         | Product Manager |  Retro done?        | Doc updated?       |
| ------------------- | --------------- | :---------: | :----------------: | :----------------: |
| Acquisition | @jeromezng  | @jstava |  |  | 
| Configure | @nicholasklick | @nagyv-gitlab  |  |  |
| Conversion | @jeromezng  | @s_awezec |  |  | 
| Create:Static Site Editor  | @jeanduplessis | @ericschurter  |  |  |
| Create:Editor       | @dsatcher + @rkuba  | @phikai  |    |   | 
| Create:Knowledge    | @dsatcher + @rkuba  | @cdybenko  |     |    | 
| Create:Source Code  | @m_gill + @andr3  | @danielgruesso  |  |  | 
| Database            | @craig-gomes | @joshlambert  |  |  | 
| Defend              | @lkerr + @thiagocsf  | @sam.white  |  |  | 
| Distribution        | @mendeni          | @ljlane  |  |  : | 
| Growth:Expansion    | @pcalder | @timhey  |  |  | 
| Growth:Retention    | @pcalder | @mkarampalas  |  |  | 
| Fulfillment         | @jameslopez + @chris_baus  | @amandarueda  |  | | 
| Geo                 | @nhxnguyen  | @fzimmer  |   |  | 
| Gitaly              | @zj-gitlab  | @jramsay  |   |  | 
| Manage:Access       | @dennis + @lmcandrew  | @mushakov + @jeremy  |   |   | 
| Manage:Analytics    | @dennis + @djensen + @wortschi      | @jeremy + @jshackelford  |   |   | 
| Manage:Compliance   | @dennis + @djensen  | @mattgonzales + @jeremy  |   |   | 
| Manage:Import       | @dennis + @lmcandrew  | @hdelalic + @jeremy  |   |   | 
| Memory              | @craig-gomes | @joshlambert  |  |  | 
| Monitor:APM         | @mnohr + @ClemMakesApps | @dhershkovitch |  |  |
| Monitor:Health      | @crystalpoole  + @ClemMakesApps | @sarahwaldner  |  |  | 
| Package             | @jhampton | @trizzi  |  | | |
| Plan                | @donaldcook + @johnhope + @jlear | @justinfarris |  |  | 
| Release:Progressive Delivery | @csouthard + @nicolewilliams | @ogolowinski  |  |  | 
| Release:Release Management | @sean_carroll + @nicolewilliams | @jmeshell  |    |  | 
| Quality             | @meks |  | | | 
| Global Search       | @changzhengliu  | @phikai + @JohnMcGuire  |  |  | 
| Secure             | @twoodham  | @stkerr + @tmccaslin + @NicoleSchwartz + @derekferguson + @matt_wilson |  | | 
| Telemetry | @jeromezng  | @sid_reddy |  |  | 
| UX                  | @vkarnes    |  |  |  | 
| Verify:CI           | @darbyfrey | @thaoyeager  |  |  | 
| Verify:Runner       | @erushton | @DarrenEastman  |  |  | 
| Verify:Testing      | @rickywiens  | @jheimbuck_gl  |  |  | 
| Ecosystem | @leipert | @deuley | | | 

cc: @fseifoddini @kencjohnston @jyavorska @david @ebrinkman @joshlambert @hilaqu 


